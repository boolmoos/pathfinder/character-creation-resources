## classes
[unchained rogue](https://www.d20pfsrd.com/classes/unchained-classes/rogue-unchained/) 4
[brawler](https://www.d20pfsrd.com/classes/hybrid-classes/brawler/) ([snakebite striker](https://www.d20pfsrd.com/classes/hybrid-classes/brawler/archetypes/paizo-brawler-archetypes/snakebite-striker)) 1

## race

halfling
- small
- **halfling luck**
  - +1 luck bonus on saving throws
- **keen senses**
  - +2 racial bonus on perception
- **underfooted dodger**
  - +5 bonus on acrobatics to move through space of larger creature
- **sure footed**
  - +2 racial bonus to acrobatics and climb

## traits
- [blessed of the norns](https://www.aonprd.com/TraitDisplay.aspx?ItemName=Blessed%20of%20the%20Norns)  
  - +2 trait bonus on perception checks to notice traps and ambushes
  - +1 trait bonus to armor class against traps and on surprise rounds while unaware
- [vexing defender](https://www.aonprd.com/TraitDisplay.aspx?ItemName=Vexing%20Defender)
  - acrobatics is a class skill
  - +1 trait bonus to acrobatics
  - +4 trait bonus on acrobatics to move through the space of a larger creature without provoking an attack of opportunity
  
## attributes

|              | **str** | **dex** | **con** | **int** | **wis** | **cha** |
|-------------:|:-------:|:-------:|:-------:|:-------:|:-------:|:-------:|
| **score**    |    9    |   20    |   16    |   10    |   10    |    9    |
| **modifier** |   -1    |   +5    |   +3    |   +0    |   +0    |   -1    |

## statistics

**hit points:** 46
**initiative:** +5
**base attack bonus:** +4
**combat maneuver defense:** 17 = 10 + 4 (base attack bonus) - 1 (strength) + 5 (dexterity) - 1 (size)

### attacks
#### unarmed strike

**damage dice:** 1d4  
**damage type:** bludgeoning  
**critical range:** 20  
**critical multiplier:** x2  
**categories:** light  
**weapon group:** close  
**notes:**
- treated as magic weapon
- can trigger *sneak attack* (see **class features**)

:::{table} standard melee attack
:widths: grid

|              |  total  | components                                                        |
|-------------:|--------:|:------------------------------------------------------------------|
| attack bonus |     +11 | 4 (base attack bonus) + 5 (strength) + 1 (enhancement) + 1 (size) |
|       damage | 1d4 + 6 | 5 (dexterity) + 1 (enhancement)                                   |

:::

### armor class

|          type         |   total   |                     components                         |
|----------------------:|:---------:|:-------------------------------------------------------|
|          **standard** |     22    | 10 + 4 (armor) + 2 (shield) + 5 (dexterity) + 1 (size) |
|             **touch** |     16    | 10 + 5 (dexterity) + 1 (size)                          |
|       **flat-footed** |     15    | 10 + 4 (armor) + 1 (size)                              |
| **flat-footed touch** |     11    | 10 + 1 (size)                                          |

**notes**
- +2 versus traps
- +1 during surpirse round while unaware

### saving throws

|     type      |   total   |            components               |
|--------------:|:---------:|:------------------------------------|
| **fortitude** |     8     |  4 + 3 (attribute) + 1 (resistance) + 1 (luck) |
|    **reflex** |    13     |  6 + 5 (attribute) + 1 (resistance) + 1 (luck) |
|      **will** |     3     |  1 + 0 (attribute) + 1 (resistance) + 1 (luck) |

**notes:**
- +1 reflex versus traps 

### skills

|                                                                                 type | total |                            components                                | class skill | untrained |
|-------------------------------------------------------------------------------------:|:-----:|:---------------------------------------------------------------------|:-----------:|:---------:|
|                 [acrobatics](https://www.aonprd.com/Skills.aspx?ItemName=Acrobatics) |  +16  | 5 (ranks) + 3 (class skill) + 5 (dexterity) + 2 (racial) + 1 (trait) |     yes     |    yes    | 
|                     [appraise](https://www.aonprd.com/Skills.aspx?ItemName=Appraise) |   +0  | 0 (ranks) + 0 (intelligence)                                         |     yes     |    yes    |
|                           [bluff](https://www.aonprd.com/Skills.aspx?ItemName=Bluff) |   +7  | 5 (ranks) + 3 (class skill) - 1 (charisma)                           |     yes     |    yes    |
|                           [climb](https://www.aonprd.com/Skills.aspx?ItemName=Climb) |   +5  | 1 (ranks) + 3 (class skill) - 1 (strength) + 2 (racial)              |     yes     |    yes    |
|                           [craft](https://www.aonprd.com/Skills.aspx?ItemName=Craft) |   +1  | 0 (ranks) + 0 (intelligence)                                         |     yes     |    yes    |
|                   [diplomacy](https://www.aonprd.com/Skills.aspx?ItemName=Diplomacy) |   -1  | 0 (ranks) - 1 (charisma)                                             |     yes     |    yes    |
|       [disable device](https://www.aonprd.com/Skills.aspx?ItemName=Disable%20Device) |  +13  | 5 (ranks) + 3 (class skill) + 5 (dexterity)                          |     yes     |     no    |
|                     [disguise](https://www.aonprd.com/Skills.aspx?ItemName=Disguise) |   -1  | 0 (ranks) - 1 (charisma)                                             |     yes     |    yes    |
|         [escape artist](https://www.aonprd.com/Skills.aspx?ItemName=Escape%20Artist) |  +13  | 5 (ranks) + 3 (class skill) + 5 (dexterity)                          |      no     |    yes    |
|                               [fly](https://www.aonprd.com/Skills.aspx?ItemName=Fly) |   +5  | 0 (ranks) + 5 (dexterity)                                            |      no     |    yes    |
|         [handle animal](https://www.aonprd.com/Skills.aspx?ItemName=Handle%20Animal) |  n/a  | 0 (ranks) + 0 (charisma)                                             |      no     |     no    |
|                             [heal](https://www.aonprd.com/Skills.aspx?ItemName=Heal) |   +0  | 0 (ranks) + 0 (wisdom)                                               |      no     |    yes    |
|                 [intimidate](https://www.aonprd.com/Skills.aspx?ItemName=Intimidate) |   -1  | 0 (ranks) - 1 (charisma)                                             |      no     |    yes    |
|          [knowledge (arcana)](https://www.aonprd.com/Skills.aspx?ItemName=Knowledge) |  n/a  | 0 (ranks) + 0 (intelligence)                                         |      no     |     no    |
|   [knowledge (dungeoneering)](https://www.aonprd.com/Skills.aspx?ItemName=Knowledge) |  n/a  | 0 (ranks) + 0 (intelligence)                                         |     yes     |     no    |
|     [knowledge (engineering)](https://www.aonprd.com/Skills.aspx?ItemName=Knowledge) |  n/a  | 0 (ranks) + 0 (intelligence)                                         |      no     |     no    |
|       [knowledge (geography)](https://www.aonprd.com/Skills.aspx?ItemName=Knowledge) |  n/a  | 0 (ranks) + 0 (intelligence)                                         |      no     |     no    |
|         [knowledge (history)](https://www.aonprd.com/Skills.aspx?ItemName=Knowledge) |  n/a  | 0 (ranks) + 0 (intelligence)                                         |      no     |     no    |
|           [knowledge (local)](https://www.aonprd.com/Skills.aspx?ItemName=Knowledge) |  n/a  | 0 (ranks) + 0 (intelligence)                                         |     yes     |     no    |
|          [knowledge (nature)](https://www.aonprd.com/Skills.aspx?ItemName=Knowledge) |  n/a  | 0 (ranks) + 0 (intelligence)                                         |      no     |     no    |
|        [knowledge (nobility)](https://www.aonprd.com/Skills.aspx?ItemName=Knowledge) |  n/a  | 0 (ranks) + 0 (intelligence)                                         |      no     |     no    |
|           [knowledge (plane)](https://www.aonprd.com/Skills.aspx?ItemName=Knowledge) |  n/a  | 0 (ranks) + 0 (intelligence)                                         |      no     |     no    |
|        [knowledge (religion)](https://www.aonprd.com/Skills.aspx?ItemName=Knowledge) |  n/a  | 0 (ranks) + 0 (intelligence)                                         |      no     |     no    |
|               [linguistics](https://www.aonprd.com/Skills.aspx?ItemName=Linguistics) |  n/a  | 0 (ranks) + 0 (intelligence)                                         |     yes     |     no    |
|                 [perception](https://www.aonprd.com/Skills.aspx?ItemName=Perception) |  +15  | 5 (ranks) + 3 (class skill) + 2 (racial) + 5 (competence)            |     yes     |    yes    |
|                       [perform](https://www.aonprd.com/Skills.aspx?ItemName=Perform) |   -1  | 0 (ranks) - 1 (charisma)                                             |     yes     |    yes    |
|                 [profession](https://www.aonprd.com/Skills.aspx?ItemName=Profession) |  n/a  | 0 (ranks) + 0 (wisdom)                                               |     yes     |     no    |
|                             [ride](https://www.aonprd.com/Skills.aspx?ItemName=Ride) |   +5  | 0 (ranks) + 5 (dexterity)                                            |      no     |    yes    |
|           [sense motive](https://www.aonprd.com/Skills.aspx?ItemName=Sense%20Motive) |   +4  | 1 (ranks) + 3 (class skill) + 0 (wisdom)                             |     yes     |    yes    |
|   [sleight of hand](https://www.aonprd.com/Skills.aspx?ItemName=Sleight%20of%20Hand) |   +9  | 1 (ranks) + 3 (class skill) + 5 (dexterity)                          |     yes     |     no    |
|                 [spellcraft](https://www.aonprd.com/Skills.aspx?ItemName=Spellcraft) |  n/a  | 0 (ranks) + 0 (intelligence)                                         |      no     |     no    |
|                       [stealth](https://www.aonprd.com/Skills.aspx?ItemName=Stealth) |  +17  | 5 (ranks) + 3 (class skill) + 5 (dexterity) + 4 (size)               |     yes     |    yes    |
|                     [survival](https://www.aonprd.com/Skills.aspx?ItemName=Survival) |   +0  | 0 (ranks) + 0 (wisdom)                                               |      no     |    yes    |
|                             [swim](https://www.aonprd.com/Skills.aspx?ItemName=Swim) |   +3  | 1 (ranks) + 3 (class skill) - 1 (strength)                           |     yes     |    yes    |
| [use magic device](https://www.aonprd.com/Skills.aspx?ItemName=Use%20Magic%20Device) |  n/a  | 0 (ranks) - 1 (charisma)                                             |     yes     |     no    |

**total:** 34  

**notes**
- acrobatics
  - +5 bonus to move through the space of a larger foe without provoking an attack of opportunity (underfoor dodger)
  - +4 trait bonus to move through the space of a larger foe wihtout provoking an attack of opportunity (vexing dodger)
  - +4 competence bonus to move through the space of a foe without provoking an attack of opportunity (belt of tumbling)
- disable device
  - +1 bonus to disable device to disable traps (trapfinding)
  - +2 competence bonus to disable device (masterwork thieves' tools)
- perception
  - +2 trait bonus to perception to notice traps (blessing of the norns)
  - +1 bonus to perception to notice traps (trapfinding)

**class features**
- [brawler's cunning](https://www.d20pfsrd.com/classes/hybrid-classes/BRAWLER/#TOC-Brawler-s-Cunning-Ex-)
- [brawler's flurry](https://www.d20pfsrd.com/classes/hybrid-classes/BRAWLER/#TOC-Brawler-s-Flurry-Ex-)
- [danger sense](https://www.d20pfsrd.com/classes/unchained-classes/Rogue-unchained/#TOC-Danger-Sense-Ex-)
- [evasion](https://www.d20pfsrd.com/classes/unchained-classes/Rogue-unchained/#TOC-Evasion-Ex-)
- [finesse training](https://www.d20pfsrd.com/classes/unchained-classes/Rogue-unchained/#TOC-Finesse-Training-Ex-)
- [martial training](https://www.d20pfsrd.com/classes/hybrid-classes/BRAWLER/#TOC-Martial-Training-Ex-)
- [sneak attack](https://www.d20pfsrd.com/classes/unchained-classes/Rogue-unchained/#TOC-Trapfinding) (3d6)
- [trapfinding](https://www.d20pfsrd.com/classes/unchained-classes/Rogue-unchained/#TOC-Trapfinding)
- [unarmed strike](https://www.d20pfsrd.com/classes/hybrid-classes/brawler/brawler#TOC-Unarmed-Strike)
- [underhanded trick](https://www.d20pfsrd.com/classes/core-classes/rogue/rogue-talents/paizo-rogue-talents/underhanded-trick/)

**feats**
- [agile maneuvers](https://www.aonprd.com/FeatDisplay.aspx?ItemName=Agile%20Maneuvers)
- [dirty fighting](https://www.aonprd.com/FeatDisplay.aspx?ItemName=Dirty%20Fighting)
- [improved dirty trick](https://www.aonprd.com/FeatDisplay.aspx?ItemName=Improved%20Dirty%20Trick)
- [improved unarmed strike](https://www.aonprd.com/FeatDisplay.aspx?ItemName=Improvedi%20Unarmed%20Strike)
- [sap adept](https://www.d20pfsrd.com/feats/combat-feats/sap-adept-combat/)
- [surprise maneuver](https://www.aonprd.com/FeatDisplay.aspx?ItemName=Surprise%20Maneuver)
- [weapon finesse](https://www.aonprd.com/FeatDisplay.aspx?ItemName=Weapon%20Finesse)

**equipment**
cloak of resistance +1                                                                                 (1000)
darkwood heavy shield                                                                                   (257)
mithral chain shirt                                                                                    (1100)
serpentine tattoo                                                                                      (2000)
[handwrap](https://www.d20pfsrd.com/equipment/weapons/weapon-descriptions/handwraps/) +1               (2301)
[belt of tumbling](https://www.d20pfsrd.com/magic-items/wondrous-items/a-b/belt-of-tumbling/)           (800)
[eyes of the eagle](https://www.d20pfsrd.com/magic-items/wondrous-items/e-g/eyes-of-the-eagle/)        (2500)


# Pre-generated characters

These characters have been written to provide a reference and basis of
comparison for players designing characters for the campaign.

1. [The arcane dancer](1/character-sheet.md)

   The arcane dancer is a melee combatant that eschews heavy armor in favor of
   favor of evasion and magical wards, blending spellcraft and swordplay to
   deliver elemental and physical damage with her sentient scimitar.

2. [The crusader](2/character-sheet.md)

   The crusader is a melee combatant favoring tactical coordinated combat, using
   his abilities to empower his comrades and hamper his foes. Outside of combat,
   the crusader is a reasonably skilled diplomat and interrogator and has
   limited access to divine magic.

3. The acrobat

   Armed with nothing but a shield and clad in light armor, the acrobat weaves
   through the battle field, confounding his foes and delivering bone-crushing
   unarmed strikes. Outside of combat, the acrobat is an expert burglar and
   reaosnably proficient scout.

4. The arrow chaplain

   Armed with the blessings of her god, bow made from horns of a great beast,
   and years of specialized martial training, the arrow chaplain threatens foes
   with a hail of arrows for almost anywhere on the battlefield. While failing
   short of the spellcasting of a full-fledged cleric of the faith, the arrow
   chaplain is a capable divine caster in her own right.

5. The big game hunter

   The big game hunter wades into battle carrying a wildly oversized axe most
   would consider better suited to a butcher's shop than to the battlefield. The
   big game hunter's specialized tactics help to organize and coordinate allies
   in combat, especially when faced with a singular threat. Outside of combat,
   the big game hunter offers the party excellent survival skills and knowledge
   of the natural world, and rudimentary scouting.

6. The bomber

   The bomber is a ranged combatant that imposes a variety of debilitating
   status effects on her foes and, provided time to prepare, can serve as
   competent sniper. Outside of combat, the bomber offers flexibility to the
   party with her ability to prepare extracts in a minute's notice in event and
   distribute beneficial effects to her allies.

3. The mage

   The mage is a primary arcane caster. In combat, the mage acts quickly to
   either impair his foes or empower his allies. The mage specializes in the
   magic missile spell, using to impart the dazed status condition on enemy
   combatants.



## classes
[magus](https://www.d20pfsrd.com/classes/base-classes/magus/) ([kensai](https://www.d20pfsrd.com/classes/base-classes/magus/archetypes/paizo-magus-archetypes/kensai/), [bladebound](https://www.d20pfsrd.com/classes/base-classes/magus/archetypes/paizo-magus-archetypes/bladebound/)) 5  

**favored class:** magus

## race

- **keen senses**
  - +2 racial bonus on perception
- **elven magic**
  - +2 racial bonus on caster level to overcome spell resistance
  - +2 racial bonus on **spellcraft** skill checks to identify the properties of magic items
- **arcane focus**
  - +2 racial bonus on concentration checks made to cast arcane spells defensively
- **low-light vision**
  - elves can see twice as far as humans in conditions of dim light
 
## traits
- [abendego spellpiercer](https://www.aonprd.com/TraitDisplay.aspx?ItemName=Abendego%20Spellpiercer)
  - +2 trait bonus on concentration checks when spellcasting  
- [stabbing spell](https://www.aonprd.com/TraitDisplay.aspx?ItemName=Stabbing%20Spells)
  - +2 trait bonus on caster level checks to overcome a foe's spell resistance until the end of your next turn after striking them with a weapon

## attributes

|              | **str** | **dex** | **con** | **int** | **wis** | **cha** |
|-------------:|:-------:|:-------:|:-------:|:-------:|:-------:|:-------:|
| **score**    |    7    |   20    |   12    |   18    |   10    |    7    |
| **modifier** |   -2    |   +5    |   +1    |   +4    |   +0    |   -2    |

## statistics

**hit points:** 36 = 26 (class) + 5 (constitution) + 5 (favored class)  
**initiative:** +5 = +5 (dexterity)  
**base attack bonus:** +3  
**combat maneuver defense:** +6  

### attacks
#### +2 blackblade (scimitar)

**damage dice:** 1d6  
**damage type:** piercing  
**critical range:** 18-20  
**critical multiplier:** x2  
**categories:** one-handed  
**weapon group:** light blades  
**special:** finesse   

:::{table} standard melee attack
:widths: grid

|              |  total  | components                                                                 |
|-------------:|--------:|:---------------------------------------------------------------------------|
| attack bonus |     +11 | 3 (base attack bonus) + 5 (dexterity) + 2 (enhancement) + 1 (weapon focus) |
|       damage | 1d6 + 7 | 5 (dexterity) + 2 (enhancement)                                            |

:::

:::{table} spell combat
:widths: grid

|              |  total  | components                                                                                    |
|-------------:|--------:|:----------------------------------------------------------------------------------------------|
| attack bonus |   +9/+9 | 3 (base attack bonus) + 5 (dexterity) + 2 (enhancement) + 1 (weapon focus) - 2 (spell combat) |
|       damage | 1d6 + 7 | 5 (dexterity) + 2 (enhancement)                                                               |

:::

:::{tip}
- The
  [blade tutor's spirit](https://www.d20pfsrd.com/magic/all-spells/b/blade-tutor-s-spirit/)
  spell can be used to negate the attack roll penalty associated with spell combat
- After confirming a critical hit but before rolling damage, consider applying
  the **perfect strike** ability.
- When faced with a difficult foe, consider using your arcane pool to increase
  the enchancment of your weapon.
- When faced with a difficult foe, consider using the **black blade strike**
  ability of your weapon. See the
  [black blade ability description](https://www.d20pfsrd.com/classes/base-classes/Magus/archetypes/paizo-magus-archetypes/bladebound/#Black_Blade_Ability_Descriptions)
  for more information.
- When faced with a foe with damage reduction you cannot readily ignore,
  consider using the **energy attunement** ability of your weapon to instead
  deal elemental damage. See the
  [black blade ability description](https://www.d20pfsrd.com/classes/base-classes/Magus/archetypes/paizo-magus-archetypes/bladebound/#Black_Blade_Ability_Descriptions)
  for more information.
:::

#### +3 keen blackblade (scimitar) (via arcane pool)

**damage dice:** 1d6  
**damage type:** piercing  
**critical range:** 15-20  
**critical multiplier:** x2  
**categories:** one-handed  
**weapon group:** light blades
**special:** finesse   

:::{table} standard melee attack
:widths: grid

|              |  total  | components                                                                 |
|-------------:|--------:|:---------------------------------------------------------------------------|
| attack bonus |     +12 | 3 (base attack bonus) + 5 (dexterity) + 2 (enhancement) + 1 (weapon focus) |
|       damage | 1d6 + 8 | 5 (dexterity) + 3 (enhancement)                                            |

:::

:::{table} spell combat
:widths: grid

|              |  total  | components                                                                                    |
|-------------:|--------:|:----------------------------------------------------------------------------------------------|
| attack bonus | +10/+10 | 3 (base attack bonus) + 5 (dexterity) + 3 (enhancement) + 1 (weapon focus) - 2 (spell combat) |
|       damage | 1d6 + 8 | 5 (dexterity) + 3 (enhancement)                                                               |

:::

:::{tip}
- The
  [blade tutor's spirit](https://www.d20pfsrd.com/magic/all-spells/b/blade-tutor-s-spirit/)
  spell can be used to negate the attack roll penalty associated with spell
  combat.
- After confirming a critical hit but before rolling damage, consider applying
  the **perfect strike** ability.
- When faced with a difficult foe, consider using the **black blade strike**
  ability of your weapon. See the
  [black blade ability description](https://www.d20pfsrd.com/classes/base-classes/Magus/archetypes/paizo-magus-archetypes/bladebound/#Black_Blade_Ability_Descriptions)
  for more information.
- When faced with a foe with damage reduction you cannot readily ignore,
  consider using the **energy attunement** ability of your weapon to instead
  deal elemental damage. See the
  [black blade ability description](https://www.d20pfsrd.com/classes/base-classes/Magus/archetypes/paizo-magus-archetypes/bladebound/#Black_Blade_Ability_Descriptions)
  for more information.
:::

### armor class

|          type         |   total   |                     components                              |
|----------------------:|:---------:|:------------------------------------------------------------|
|          **standard** |     25    | 10 + 4 (armor) + 5 (dexterity) + 4 (dodge) + 1 (deflection) |
|             **touch** |     20    | 10 + 5 (dexterity) + 4 (dodge) + 1 (deflection)             |
|       **flat-footed** |     16    | 10 + 4 (armor) + 1 (deflection)                             |
| **flat-footed touch** |     11    | 10 + 1 (deflection)                                         |

### saving throws

|     type      |   total   |            components               |
|--------------:|:---------:|:------------------------------------|
| **fortitude** |     6     |  4 + 1 (attribute) + 1 (resistance) |
|    **reflex** |     7     |  1 + 5 (attribute) + 1 (resistance) |
|      **will** |     5     |  4 + 0 (attribute) + 1 (resistance) |

## skills

**total:** 30 = 10 (class) + 20 (intelligence)

|                                                                                 type | total |                    components                                | class skill | untrained |
|-------------------------------------------------------------------------------------:|:-----:|:-------------------------------------------------------------|:-----------:|:---------:|
|                 [acrobatics](https://www.aonprd.com/Skills.aspx?ItemName=Acrobatics) |   +5  | 0 (ranks) + 5 (dexterity)                                    |      no     |    yes    | 
|                     [appraise](https://www.aonprd.com/Skills.aspx?ItemName=Appraise) |   +4  | 0 (ranks) + 4 (intelligence)                                 |      no     |    yes    |
|                           [bluff](https://www.aonprd.com/Skills.aspx?ItemName=Bluff) |   -2  | 0 (ranks) - 2 (charisma)                                     |      no     |    yes    |
|                           [climb](https://www.aonprd.com/Skills.aspx?ItemName=Climb) |   +2  | 1 (ranks) - 2 (strength) + 3 (class skill)                   |     yes     |    yes    |
|                           [craft](https://www.aonprd.com/Skills.aspx?ItemName=Craft) |   +4  | 0 (ranks) + 4 (intelligence)                                 |     yes     |    yes    |
|                   [diplomacy](https://www.aonprd.com/Skills.aspx?ItemName=Diplomacy) |   -2  | 0 (ranks) - 2 (charisma)                                     |      no     |    yes    |
|       [disable device](https://www.aonprd.com/Skills.aspx?ItemName=Disable%20Device) |  n/a  | 0 (ranks) + 5 (dexterity)                                    |      no     |     no    |
|                     [disguise](https://www.aonprd.com/Skills.aspx?ItemName=Disguise) |   -2  | 0 (ranks) - 2 (charisma)                                     |      no     |    yes    |
|         [escape artist](https://www.aonprd.com/Skills.aspx?ItemName=Escape%20Artist) |   +5  | 0 (ranks) + 5 (dexterity)                                    |      no     |    yes    |
|                               [fly](https://www.aonprd.com/Skills.aspx?ItemName=Fly) |   +5  | 0 (ranks) + 5 (dexterity)                                    |     yes     |    yes    |
|         [handle animal](https://www.aonprd.com/Skills.aspx?ItemName=Handle%20Animal) |  n/a  | 0 (ranks) - 2 (charisma)                                     |      no     |     no    |
|                             [heal](https://www.aonprd.com/Skills.aspx?ItemName=Heal) |   +0  | 0 (ranks) + 0 (wisdom)                                       |      no     |    yes    |
|                 [intimidate](https://www.aonprd.com/Skills.aspx?ItemName=Intimidate) |   -2  | 0 (ranks) - 2 (charisma)                                     |     yes     |    yes    |
|          [knowledge (arcana)](https://www.aonprd.com/Skills.aspx?ItemName=Knowledge) |  +12  | 5 (ranks) + 4 (intelligence) + 3 (class skill)               |     yes     |     no    |
|   [knowledge (dungeoneering)](https://www.aonprd.com/Skills.aspx?ItemName=Knowledge) |  +12  | 5 (ranks) + 4 (intelligence) + 3 (class skill)               |     yes     |     no    |
|     [knowledge (engineering)](https://www.aonprd.com/Skills.aspx?ItemName=Knowledge) |  n/a  | 0 (ranks) + 4 (intelligence)                                 |      no     |     no    |
|       [knowledge (geography)](https://www.aonprd.com/Skills.aspx?ItemName=Knowledge) |  n/a  | 0 (ranks) + 4 (intelligence)                                 |      no     |     no    |
|         [knowledge (history)](https://www.aonprd.com/Skills.aspx?ItemName=Knowledge) |  n/a  | 0 (ranks) + 4 (intelligence)                                 |      no     |     no    |
|           [knowledge (local)](https://www.aonprd.com/Skills.aspx?ItemName=Knowledge) |  n/a  | 0 (ranks) + 4 (intelligence)                                 |      no     |     no    |
|          [knowledge (nature)](https://www.aonprd.com/Skills.aspx?ItemName=Knowledge) |  n/a  | 0 (ranks) + 4 (intelligence)                                 |      no     |     no    |
|        [knowledge (nobility)](https://www.aonprd.com/Skills.aspx?ItemName=Knowledge) |  n/a  | 0 (ranks) + 4 (intelligence)                                 |      no     |     no    |
|           [knowledge (plane)](https://www.aonprd.com/Skills.aspx?ItemName=Knowledge) |  +12  | 5 (ranks) + 4 (intelligence) + 3 (class skill)               |     yes     |     no    |
|        [knowledge (religion)](https://www.aonprd.com/Skills.aspx?ItemName=Knowledge) |  n/a  | 0 (ranks) + 4 (intelligence)                                 |      no     |     no    |
|               [linguistics](https://www.aonprd.com/Skills.aspx?ItemName=Linguistics) |  n/a  | 0 (ranks) + 4 (intelligence)                                 |      no     |     no    |
|                 [perception](https://www.aonprd.com/Skills.aspx?ItemName=Perception) |   +7  | 5 (ranks) + 0 (wisdom) + 2 (racial)                          |      no     |    yes    |
|               [perform](https://www.aonprd.com/Skills.aspx?ItemName=Perform) (dance) |   +3  | 2 (ranks) - 2 (charisma) + 3 (class skill)                   |      no     |    yes    |
|                 [profession](https://www.aonprd.com/Skills.aspx?ItemName=Profession) |  n/a  | 0 (ranks) + 0 (wisdom)                                       |     yes     |     no    |
|                             [ride](https://www.aonprd.com/Skills.aspx?ItemName=Ride) |   +5  | 0 (ranks) + 5 (dexterity)                                    |     yes     |    yes    |
|           [sense motive](https://www.aonprd.com/Skills.aspx?ItemName=Sense%20Motive) |   +0  | 0 (ranks) + 0 (wisdom)                                       |      no     |    yes    |
|   [sleight of hand](https://www.aonprd.com/Skills.aspx?ItemName=Sleight%20of%20Hand) |  n/a  | 0 (ranks) + 5 (dexterity)                                    |      no     |     no    |
|                 [spellcraft](https://www.aonprd.com/Skills.aspx?ItemName=Spellcraft) |  +12  | 5 (ranks) + 4 (intelligence) + 3 (class skill)               |     yes     |     no    |
|                       [stealth](https://www.aonprd.com/Skills.aspx?ItemName=Stealth) |   +5  | 0 (ranks) + 5 (dexterity)                                    |      no     |    yes    |
|                     [survival](https://www.aonprd.com/Skills.aspx?ItemName=Survival) |   +0  | 0 (ranks) + 0 (wisdom)                                       |      no     |    yes    |
|                             [swim](https://www.aonprd.com/Skills.aspx?ItemName=Swim) |   +3  | 2 (ranks) - 2 (strength) + 3 (class skill)                   |     yes     |    yes    |
| [use magic device](https://www.aonprd.com/Skills.aspx?ItemName=Use%20Magic%20Device) |   -2  | 0 (ranks) - 2 (charisma)                                     |     yes     |     no    |

:::{note}
- **perception**
  - +2 while wielding your blackblade
- **sense motive**
  - +2 while wielding your blackblade
- **spellcraft**
  - +2 to identify the properties of magic items
:::

## class features

### spell casting

|          level | 0 | 1 | 2 |
|---------------:|:-:|:-:|:-:|
| spells per day | 4 | 4 | 2 |

#### spells known

##### 0th level

- [acid splash](https://www.d20pfsrd.com/magic/all-spells/a/acid-splash/)
- [arcane mark](https://www.d20pfsrd.com/magic/all-spells/a/arcane-mark/)
- [dancing lights](https://www.d20pfsrd.com/magic/all-spells/d/dancing-lights/)
- [daze](https://www.d20pfsrd.com/magic/all-spells/d/daze/)
- [detect fiendish presence](https://www.d20pfsrd.com/magic/all-spells/d/detect-fiendish-presence/)
- [detect magic](https://www.d20pfsrd.com/magic/all-spells/d/detect-magic/)
- [disrupt undead](https://www.d20pfsrd.com/magic/all-spells/disrupt-unded//)
- [flare](https://www.d20pfsrd.com/magic/all-spells/f/flare/)
- [ghost step](https://www.d20pfsrd.com/magic/all-spells/g/ghost-step/)
- [grasp](https://www.d20pfsrd.com/magic/all-spells/g/grasp/)
- [light](https://www.d20pfsrd.com/magic/all-spells/l/light/)
- [mage hand](https://www.d20pfsrd.com/magic/all-spells/m/mage-hand/)
- [open/close](https://www.d20pfsrd.com/magic/all-spells/o/open-close/)
- [prestidigitation](https://www.d20pfsrd.com/magic/all-spells/p/prestidigitation/)
- [ray of frost](https://www.d20pfsrd.com/magic/all-spells/r/ray-of-frost/)
- [read magic](https://www.d20pfsrd.com/magic/all-spells/r/read-magic/)
- [spark](https://www.d20pfsrd.com/magic/all-spells/s/spark/)
- [touch of fatigue](https://www.d20pfsrd.com/magic/all-spells/t/touch-of-fatigue/)

##### 1st level

- [blade tutor's spirit](https://www.d20pfsrd.com/magic/all-spells/b/blade-tutor-s-spirit/)
- [chill touch](https://www.d20pfsrd.com/magic/all-spells/c/chill-touch/)
- [corrosive touch](https://www.d20pfsrd.com/magic/all-spells/c/corrosive-touch/)
- [frostbite](https://www.d20pfsrd.com/magic/all-spells/f/frostbite/)
- [infernal healing](https://www.d20pfsrd.com/magic/all-spells/i/infernal-healing/)
- [long arm](https://www.d20pfsrd.com/magic/all-spells/l/long-arm/)
- [mage armor](https://www.d20pfsrd.com/magic/all-spells/m/mage-armor/),
- [shield](https://www.d20pfsrd.com/magic/all-spells/s/shield/)
- [shocking grasp](https://www.d20pfsrd.com/magic/all-spells/s/shocking-grasp/)
- [true strike](https://www.d20pfsrd.com/magic/all-spells/t/true-strike/)
- [weaponwand](https://www.d20pfsrd.com/magic/all-spells/i/w/weaponwand)
- [windy escape](https://www.d20pfsrd.com/magic/all-spells//w/windy-escape)

##### 2nd level

- [frigid touch](https://www.d20pfsrd.com/magic/all-spells/f/frigid-touch/)
- [invisibility](https://www.d20pfsrd.com/magic/all-spells/i/invisibility/)
- [mirror image](https://www.d20pfsrd.com/magic/all-spells/m/mirror-image/)
- [page-bound epiphany](https://www.d20pfsrd.com/magic/all-spells/p/page-bound-epiphany/)

:::{tip}

In general, because the **frostbite** spell allows the caster to deliver
multiple touch attacks, it's the preferred spell for use with the
**spell combat** and **spell strike** on a typical adventuring day. It allows
the magus to stretch their limited spell casting resources over more encounters.

That said, other touch spells may be better under certain conditions

- **chill touch** when undead are anticipated
- **corrosive touch** when high spell resistance is anticipated
- **shocking grap** when anticipating a small number of highly threatening enemies 

:::

#### Spells prepared

##### 0th level

- [detect magic](https://www.d20pfsrd.com/magic/all-spells/d/detect-magic/)
- [mage hand](https://www.d20pfsrd.com/magic/all-spells/m/mage-hand/)
- [read magic](https://www.d20pfsrd.com/magic/all-spells/r/read-magic/)
- [touch of fatigue](https://www.d20pfsrd.com/magic/all-spells/t/touch-of-fatigue/)

##### 1st level

- [blade tutor's spirit](https://www.d20pfsrd.com/magic/all-spells/b/blade-tutor-s-spirit/)
- [frostbite](https://www.d20pfsrd.com/magic/all-spells/f/frostbite/)
- [mage armor](https://www.d20pfsrd.com/magic/all-spells/m/mage-armor/),
- [shield](https://www.d20pfsrd.com/magic/all-spells/s/shield/)

##### 2nd level

- [frigid touch](https://www.d20pfsrd.com/magic/all-spells/f/frigid-touch/)
- [mirror image](https://www.d20pfsrd.com/magic/all-spells/m/mirror-image/)

## feats

- [combat casting](https://www.d20pfsrd.com/feats/general-feats/combat-casting/)
  - +4 bonus to concentration checks to cast defensively or while grappled
- [dervish dance](https://www.d20pfsrd.com/feats/combat-feats/dervish-dance-combat/)
  - You may use your dexterity modifier in place of your strength modifier on
    melee attacks and damage rolls when wielding a scimitar in one hand.
  - You may treat a scimitar as a one-handed piercing weapon for all feats and
    class abilities that require such a weapon.
  - You gain no benefit from this feat if you are carrying a weapon or shield in
    your offhand.
- [extra arcana](https://www.d20pfsrd.com/feats/general-feats/extra-arcana/):
  [spell blending](https://www.d20pfsrd.com/classes/base-classes/magus/magus-arcana/paizo-magus-arcana/spell-blending-ex/)
  ([mage armor](https://www.d20pfsrd.com/magic/all-spells/m/mage-armor/),
   [touch of fatigue](https://www.d20pfsrd.com/magic/all-spells/t/touch-of-fatigue/))
- [weapon finesse](https://www.d20pfsrd.com/feats/combat-feats/weapon-finesse-combat)
  - You may add your dexterity bonus in place of your strength modifier for
    attack rolls when using a weapon with the finesse property.
- [weapon focus](https://www.d20pfsrd.com/feats/combat-feats/weapon-focus-combat): scimitar
  - +1 to attack rolls with scimitar weapons.

## class features

- [arcane pool](https://www.d20pfsrd.com/classes/Base-classes/Magus/#TOC-Arcane-Pool-Su-) (5 points)
  - Spend 1 point from the arcane pool, increase the enhancement bonus of a
    weapon you're holding by 2.
  - In leui of enchancement bonus, the magus can instead add any of the
    following weapon properties:

    - [dancing](https://www.d20pfsrd.com/magic-items/magic-weapons/magic-weapon-special-abilities/dancing)
    - [flaming](https://www.d20pfsrd.com/magic-items/magic-weapons/magic-weapon-special-abilities/flaming)
    - [flaming burst](https://www.d20pfsrd.com/magic-items/magic-weapons/magic-weapon-special-abilities/flaming-burst)
    - [frost](https://www.d20pfsrd.com/magic-items/magic-weapons/magic-weapon-special-abilities/frost)
    - [icy burst](https://www.d20pfsrd.com/magic-items/magic-weapons/magic-weapon-special-abilities/icy-burst)
    - [keen](https://www.d20pfsrd.com/magic-items/magic-weapons/magic-weapon-special-abilities/keen)
    - [shock](https://www.d20pfsrd.com/magic-items/magic-weapons/magic-weapon-special-abilities/shock)
    - [shocking burst](https://www.d20pfsrd.com/magic-items/magic-weapons/magic-weapon-special-abilities/shocking-burst)
    - [speed](https://www.d20pfsrd.com/magic-items/magic-weapons/magic-weapon-special-abilities/speed)
    - [vorpal](https://www.d20pfsrd.com/magic-items/magic-weapons/magic-weapon-special-abilities/vorpal)

    Adding these properties consume an amount of bonus equal to the property's
    base price modifier.

- [black blade](https://www.d20pfsrd.com/classes/base-classes/Magus/archetypes/paizo-magus-archetypes/bladebound/#Black_Blade_Ex)
  - A bladebound has a powerful sentient magical weapon called a black blade.
  - See the linked page for more information.

- [bonus feat](https://www.d20pfsrd.com/classes/Base-classes/Magus/#TOC-Bonus-Feats)
  - At 5th level and every six levels thereafter, a magus gains a bonus feat.
  - This feat must be either an item creation feat, a metamagic feat, or a
    combat feat
  - The magus must meet the prerequisites for this feat as normal

- [canny defense](https://www.d20pfsrd.com/classes/base-classes/Magus/archetypes/paizo-magus-archetypes/kensai/#Canny_Defense_Ex)
  - When wearing light or no armor, a kensai adds her intelligence bonus (to a
    maxinum of her class level) to her armor class as a dodge bonus.

- [cantrips](https://www.d20pfsrd.com/classes/Base-classes/Magus/#TOC-Cantrips-)

- [perfect strike](https://www.d20pfsrd.com/classes/base-classes/Magus/archetypes/paizo-magus-archetypes/kensai/#Perfect_Strike_Ex)
  - When a kensai hits with her chosen weapon, she can choose to spend 1 point
    from her arcane pool to maximize the damage from her weapon dice.
  - If the kensai confirms a critical hit, she may instead choose to spend 2
    points from her arcane pool to increase her weapons critical multipler by
    1.

- [spell combat](https://www.d20pfsrd.com/classes/Base-classes/Magus/#TOC-Spell-Combat-Ex-)
  - As a full round action, a magus may cast a spell with casting time of one
    standard action and perform a full attack with a light or one-handed weapon.
    A magus must have a free hand to perform this action.
  - All attacks made as part of this action are subject to a -2 penalty.

- [spellstrike](https://www.d20pfsrd.com/classes/Base-classes/Magus/#TOC-Spellstrike-Su-)
  - A magus may make an attack with a weapon they're wielding to deliver the
    effect of a spell with a range of "touch" in place of the touch attack 
    granted by the spell.

- [weapon focus](https://www.d20pfsrd.com/classes/base-classes/Magus/archetypes/paizo-magus-archetypes/kensai/#Weapon_Focus_Ex)
  - A kensai gains weapon focus with her chosen weapon as a bonus feat


## equipment

|                                                                                           equipment                                                                                             | quantity |      value       |
|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------:|:--------:|-----------------:|
| [bag of holding](https://www.d20pfsrd.com/magic-items/wondrous-items/a-b/bag-of-holding/) (I)                                                                                                   |     1    | 2500 gold pieces |
| +2 blackblade scimitar                                                                                                                                                                          |     1    | N/A              |
| +1 [cloak of resistance](https://www.d20pfsrd.com/magic-items/wondrous-items/c-d/cloak-of-resistance/)                                                                                          |     1    | 1000 gold pieces |
| [pearl of power](https://www.d20pfsrd.com/magic-items/wondrous-items/m-q/pearl-of-power/) (1st)                                                                                                 |     3    | 3000 gold pieces |
| +1 [ring of protection](https://www.d20pfsrd.com/magic-items/rings/ring-of-protection/)                                                                                                         |     1    | 2000 gold pieces |
| spellbook, compact                                                                                                                                                                              |     1    |   50 gold pieces |



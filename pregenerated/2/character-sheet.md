## classes  
[fighter](https://www.d20pfsrd.com/classes/core-classes/fighter/) ([drill sergeant](https://www.d20pfsrd.com/classes/core-classes/fighter/archetypes/paizo-fighter-archetypes/drill-sergeant-fighter-archetype/)) 2  
[inquisitor](https://www.d20pfsrd.com/Classes/base-Classes/inquisitor/) ([sanctified slayer](https://www.d20pfsrd.com/classes/base-classes/inquisitor/archetypes/inquisitor-archetypes-paizo/sanctified-slayer)) 3

**favored class:** inquisitor

## race

dwarf
- medium
- **darkvision** 60 ft
- **slow and steady**
  - 20ft move speed
  - your speed is never modified by armor or encumberance
- **defensive training**
  - +4 dodge bonus to their armor class against creatures with the giant subtype
- **giant hunter**
  - +1 bonus on attack rolls against humanoids of the gaint subtype  
  - +2 bonus on survival to track such creatures
- **hardy**
  - +2 on saving throws against poison, spells, and spell-like abilities
- **stability**
  - +4 racial bonus to combat maneuver defense when resisting a bull rush and
    trip while standing on the ground
- **stonecutting**
  - +2 racial to perception to notice traps or hidden doors in stone walls or
    floors
- **weapon familiarity**
  - proficient with battle axes, picks, and warhammers.  
  - weapons with dwarven in the name are treated as martial weapons

## trait
- [militia](https://www.aonprd.com/TraitDisplay.aspx?ItemName=Militiaa)  
  +1 trait bonus on attacks made while flanking an opponent
- [heirloom weapon](https://www.d20pfsrd.com/traits/equipment-traits/heirloom-weapon/)  
  +2 trait bonus on trip attempts using this weapon

## attributes

|              | **str** | **dex** | **con** | **int** | **wis** | **cha** |
|-------------:|:-------:|:-------:|:-------:|:-------:|:-------:|:-------:|
| **score**    |   16    |   14    |   16    |   12    |   16    |    5    |
| **modifier** |   +3    |   +2    |   +3    |   +1    |   +3    |   -3    |

## statistics

**hit points:** 44 = 29 (class) + 15 (constitution)  
**initiative:** +5 = +2 (dexterity) + 3 (cunning initiative)  
**base attack bonus:** +4  
**combat maneuver defense:** 19 = 10 + 4 (base attack bonus) + 3 (strength) + 2 (dexterity)
- +4 versus bull rush and trip when touching the ground
- +2 versus trip

### attacks

#### masterwork dwarven giant-sticker

**damage dice:** 2d6  
**damage type:** piercing or slashing  
**critical range:** 20  
**critical multiplier:** x3  
**categories:** two-handed  
**weapon group:** polearms  
**special:** brace, reach

:::{table} standard melee atack
:widths: grid

|              |  total  | components                                             |
|-------------:|--------:|:-------------------------------------------------------|
| attack bonus |      +8 | 4 (base attack bonus) + 3 (strength) + 1 (enhancement) |
|       damage | 2d6 + 5 | 4 (1.5 strength) + 1 (enhancement)                     |

:::

:::{table} power atack
:widths: grid

|              |   total  | components                                                                |
|-------------:|---------:|:--------------------------------------------------------------------------|
| attack bonus |       +6 | 4 (base attack bonus) + 3 (strength) + 1 (enhancement) - 2 (power attack) |
|       damage | 2d6 + 11 | 4 (1.5 strength) + 6 (power attack) + 1 (enhancement)                     |

:::

:::{table} trip
:widths: grid

|                |   total  |                                       components                                                 |
|---------------:|---------:|:-------------------------------------------------------------------------------------------------|
| maneuver bonus |      +12 | 4 (base attack bonus) + 3 (strength) + 1 (enhancement) + 2 (improved trip) + 2 (heirloom weapon) |

:::

:::{note}
- roll twice, taking the better value when making a trip attack when at least
  one other ally threatened the target (see the *tandem trip* feat and
  *solo tactics* class feature)
- +4 attack on attacks of opportunity when at last one other ally threatens the
  target (see the *paired opportunist* feat and the *solo tactics* class
  feature)
- +3 attack on standard and power attacks against flanked enemy (see the
  *militia* trait)
- +5 attack on trip against flanked enemy (see the *militia* trait and the
  *dirty fighting* feat)
- +1 attack and damage versus studied enemy (see the *studied target* class
  features)
:::

:::{tip}
- trip attack may be used against adjacent enemies (see the *close sweep*
  polearm trick from the *weapon trick* feat)
- in scenarios where trip attacks would not be advantageous, consider using the
  inquisitor's *teamwork feats* class feature to exchange the *tandem trip*
  teamwork feat for the
  [outflank](https://www.d20pfsrd.com/feats/combat-feats/outflank-combat-teamwork/)
  teamwork feat 
:::

#### masterwork cold iron sphinx hammer

**damage dice:** 1d10  
**damage type:** bludgeoning  
**range increment:** 20 ft  
**critical range:** 20  
**critical multiplier:** x3  
**categories:** two-handed  
**weapon group:** hammers, thrown

:::{table} standard melee atack
:widths: grid

|              |   total  | components                                             |
|-------------:|---------:|:-------------------------------------------------------|
| attack bonus |       +8 | 4 (base attack bonus) + 3 (strength) + 1 (enhancement) |
|       damage | 1d10 + 4 | 4 (1.5 strength)                                       |

:::

:::{table} power atack
:widths: grid

|              |    total  | components                                                                |
|-------------:|----------:|:--------------------------------------------------------------------------|
| attack bonus |        +6 | 4 (base attack bonus) + 3 (strength) + 1 (enhancement) - 2 (power attack) |
|       damage | 1d10 + 10 | 4 (1.5 strength) 6 (power attack)                                         |

:::

:::{table} thrown attack
:widths: grid

|              |   total  | components                                             |
|-------------:|---------:|:-------------------------------------------------------|
| attack bonus |       +7 | 4 (base attack bonus) + 2 (strength) + 1 (enhancement) |
|       damage | 1d10 + 3 | 4 (strength)                                           |

:::

:::{note}
- throwing a two-handed weapon is a full-round action
:::

:::{tip}
- if anticipating use as a thrown weapon, consider applying a scroll of
  *returning weapon* (see equipment)
:::

### armor class

|          type         |   total   |                  components                     |
|----------------------:|:---------:|:------------------------------------------------|
|          **standard** |     22    | 10 + 9 (armor) + 2 (dexterity) + 1 (deflection) |
|             **touch** |     13    | 10 + 2 (dexterity) + 1 (deflection)             |
|       **flat-footed** |     20    | 10 + 9 (armor) + 1 (deflection)                 |
| **flat-footed touch** |     11    | 10 + 1 (deflection)                             |

:::{note} 
- +4 dodge bonus base and touch armor class against creatures with the giant subtype
:::

### saving throws

|     type      |   total   |            components               |
|--------------:|:---------:|:------------------------------------|
| **fortitude** |    10     |  6 + 3 (attribute) + 1 (resistance) |
|    **reflex** |     4     |  1 + 2 (attribute) + 1 (resistance) |
|      **will** |     7     |  3 + 3 (attribute) + 1 (resistance) |

:::{note}  
- +2 on all saves versus spells, spell-like abilities, and poison
:::

### skills

**total:** 30 = 22 (class) + 5 (intelligence) + 3 (favored class)

|                                                                                 type | total |                    components                                                     | class skill | untrained |
|-------------------------------------------------------------------------------------:|:-----:|:----------------------------------------------------------------------------------|:-----------:|:---------:|
|                 [acrobatics](https://www.aonprd.com/Skills.aspx?ItemName=Acrobatics) |   -4  | 0 (ranks) + 2 (dexterity) - 6 (armor check)                                       |      no     |    yes    | 
|                     [appraise](https://www.aonprd.com/Skills.aspx?ItemName=Appraise) |   +1  | 0 (ranks) + 1 (intelligence)                                                      |      no     |    yes    |
|                           [bluff](https://www.aonprd.com/Skills.aspx?ItemName=Bluff) |   -3  | 0 (ranks) - 3 (charisma)                                                          |     yes     |    yes    |
|                           [climb](https://www.aonprd.com/Skills.aspx?ItemName=Climb) |   +1  | 1 (ranks) + 3 (class skill) + 3 (strength) - 6 (armor check)                      |     yes     |    yes    |
|                           [craft](https://www.aonprd.com/Skills.aspx?ItemName=Craft) |   +1  | 0 (ranks) + 1 (intelligence)                                                      |     yes     |    yes    |
|                   [diplomacy](https://www.aonprd.com/Skills.aspx?ItemName=Diplomacy) |  +11  | 5 (ranks) + 3 (class skill) + 3 (inspired rhetoric)                               |     yes     |    yes    |
|       [disable device](https://www.aonprd.com/Skills.aspx?ItemName=Disable%20Device) |  n/a  | 0 (ranks) + 2 (dexterity) - 6 (armor check)                                       |      no     |     no    |
|                     [disguise](https://www.aonprd.com/Skills.aspx?ItemName=Disguise) |   -3  | 0 (ranks) - 3 (charisma)                                                          |     yes     |    yes    |
|         [escape artist](https://www.aonprd.com/Skills.aspx?ItemName=Escape%20Artist) |   -4  | 0 (ranks) + 2 (dexterity) - 6 (armor check)                                       |      no     |    yes    |
|                               [fly](https://www.aonprd.com/Skills.aspx?ItemName=Fly) |   -4  | 0 (ranks) + 2 (dexterity) - 6 (armor check)                                       |      no     |    yes    |
|         [handle animal](https://www.aonprd.com/Skills.aspx?ItemName=Handle%20Animal) |  n/a  | 0 (ranks) - 3 (charisma)                                                          |     yes     |     no    |
|                             [heal](https://www.aonprd.com/Skills.aspx?ItemName=Heal) |   +7  | 1 (ranks) + 3 (class skill) + 3 (wisdom)                                          |     yes     |    yes    |
|                 [intimidate](https://www.aonprd.com/Skills.aspx?ItemName=Intimidate) |  +17  | 5 (ranks) + 3 (class skill) + 3 (inspired rhetoric) + 1 (morale) + 5 (competence) |     yes     |    yes    |
|          [knowledge (arcana)](https://www.aonprd.com/Skills.aspx?ItemName=Knowledge) |   +5  | 1 (ranks) + 3 (class skill) + 1 (intelligence)                                    |     yes     |     no    |
|   [knowledge (dungeoneering)](https://www.aonprd.com/Skills.aspx?ItemName=Knowledge) |   +5  | 1 (ranks) + 3 (class skill) + 1 (intelligence)                                    |     yes     |     no    |
|     [knowledge (engineering)](https://www.aonprd.com/Skills.aspx?ItemName=Knowledge) |  n/a  | 0 (ranks) + 1 (intelligence)                                                      |     yes     |     no    |
|       [knowledge (geography)](https://www.aonprd.com/Skills.aspx?ItemName=Knowledge) |  n/a  | 0 (ranks) + 1 (intelligence)                                                      |      no     |     no    |
|         [knowledge (history)](https://www.aonprd.com/Skills.aspx?ItemName=Knowledge) |  n/a  | 0 (ranks) + 1 (intelligence)                                                      |      no     |     no    |
|           [knowledge (local)](https://www.aonprd.com/Skills.aspx?ItemName=Knowledge) |  n/a  | 0 (ranks) + 1 (intelligence)                                                      |      no     |     no    |
|          [knowledge (nature)](https://www.aonprd.com/Skills.aspx?ItemName=Knowledge) |   +5  | 1 (ranks) + 3 (class skill) + 1 (intelligence)                                    |     yes     |     no    |
|        [knowledge (nobility)](https://www.aonprd.com/Skills.aspx?ItemName=Knowledge) |  n/a  | 0 (ranks) + 1 (intelligence)                                                      |      no     |     no    |
|           [knowledge (plane)](https://www.aonprd.com/Skills.aspx?ItemName=Knowledge) |   +5  | 1 (ranks) + 3 (class skill) + 1 (intelligence)                                    |     yes     |     no    |
|        [knowledge (religion)](https://www.aonprd.com/Skills.aspx?ItemName=Knowledge) |   +5  | 1 (ranks) + 3 (class skill) + 1 (intelligence)                                    |     yes     |     no    |
|               [linguistics](https://www.aonprd.com/Skills.aspx?ItemName=Linguistics) |  n/a  | 0 (ranks) + 1 (intelligence)                                                      |      no     |     no    |
|                 [perception](https://www.aonprd.com/Skills.aspx?ItemName=Perception) |  +11  | 5 (ranks) + 3 (class skill) + 3 (wisdom)                                          |     yes     |    yes    |
|                       [perform](https://www.aonprd.com/Skills.aspx?ItemName=Perform) |   -3  | 0 (ranks) - 3 (charisma)                                                          |      no     |    yes    |
|                 [profession](https://www.aonprd.com/Skills.aspx?ItemName=Profession) |  n/a  | 0 (ranks) + 3 (wisdom)                                                            |     yes     |     no    |
|                             [ride](https://www.aonprd.com/Skills.aspx?ItemName=Ride) |   -4  | 0 (ranks) + 2 (dexterity) - 6 (armor check)                                       |     yes     |    yes    |
|           [sense motive](https://www.aonprd.com/Skills.aspx?ItemName=Sense%20Motive) |  +12  | 5 (ranks) + 3 (class skill) + 3 (wisdom) + 1 (morale)                             |     yes     |    yes    |
|   [sleight of hand](https://www.aonprd.com/Skills.aspx?ItemName=Sleight%20of%20Hand) |  n/a  | 0 (ranks) + 2 (dexterity) - 6 (armor check)                                       |      no     |     no    |
|                 [spellcraft](https://www.aonprd.com/Skills.aspx?ItemName=Spellcraft) |  n/a  | 0 (ranks) + 1 (intelligence)                                                      |     yes     |     no    |
|                       [stealth](https://www.aonprd.com/Skills.aspx?ItemName=Stealth) |   -4  | 0 (ranks) + 2 (dexterity) - 6 (armor check)                                       |     yes     |    yes    |
|                     [survival](https://www.aonprd.com/Skills.aspx?ItemName=Survival) |   +9  | 3 (ranks) + 3 (class skill) + 3 (wisdom)                                          |     yes     |    yes    |
|                             [swim](https://www.aonprd.com/Skills.aspx?ItemName=Swim) |   -3  | 0 (ranks) + 3 (strength) - 6 (armor check)                                        |     yes     |    yes    |
| [use magic device](https://www.aonprd.com/Skills.aspx?ItemName=Use%20Magic%20Device) |  n/a  | 0 (ranks) - 3 (charisma)                                                          |      no     |     no    |

:::{note}
- knowledge (arcana)
  - add your wisdom modifier (+3) to checks to identify the abilities and
    weaknesses of monsters
- knowledge (dungeoneering)
  - add your wisdom modifier (+3) to checks to identify the abilities and
    weaknesses of monsters
- knowledge (nature)
  - add your wisdom modifier (+3) to checks to identify the abilities and
    weaknesses of monsters
- knowledge (planes)
  - add your wisdom modifier (+3) to checks to identify the abilities and
    weaknesses of monsters
- knowledge (religion)
  - add your wisdom modifier (+3) to checks to identify the abilities and
    weaknesses of monsters
- perception
  - +2 racial to notice traps or hidden doors in stone walls or floors
- survival
  - add half your inquisitor level (+1) made to follow or identify tracks
  - add +2 to survival to track creatures with the giant subtype
:::

## class features

- [cunning initiative](https://www.d20pfsrd.com/classes/base-classes/Inquisitor/#TOC-Cunning-Initiative-Ex-)  
  add your wisdom modifier in addition to your dexterity modifier to your
  initiative
- [detect alignment](https://www.d20pfsrd.com/classes/base-classes/Inquisitor/#TOC-Detect-Alignment-Sp-)  
  at will- detect chaos, detect evil, detect good, detect law
- [monster lore](https://www.d20pfsrd.com/Classes/base-Classes/inquisitor/#TOC-Monster-Lore-Ex-)  
  add your wisdom modifier in addition to your intelligence modifier when making
  skill checks to identify the abilities and weaknesses of creatures
- [reformation inquisition](https://www.d20pfsrd.com/classes/base-classes/inquisitor/inquisitions/inquistions-paizo/reformation-inquisition/)  
  - **inspired rhetoric**  
    use your wisdom modifier in place of your charisma modifier when making
    diplomacy, intimidate, and perform (oratory) checks.
- [solo tactics](https://www.d20pfsrd.com/classes/base-classes/Inquisitor/#TOC-Solo-Tactics-Ex-)  
  your allies are treated as if they possesssed the same teamwork feats you do
  for the purpose determining whether you receive ~~a bonus from~~ the benefit
  of those teamwork feats
- [spells](https://www.d20pfsrd.com/classes/base-classes/Inquisitor/#Spells)
- [stern gaze](https://www.d20pfsrd.com/Classes/base-Classes/inquisitor/#TOC-Stern-Gaze-Ex-)  
  you gain a morale bonus to all intimidate and sense motive checks equal to
  half your inquisitor level rounded down 
- [studied target](https://www.d20pfsrd.com/classes/hybrid-classes/SLAYER/#Studied_Target_Ex) +1
  - you can study a target as a move action
  - you gain your studied target bonus to bluff, knowledge, perception, sense
    motive, and survival checks attempted against the target
  - you gain your studied target bonus as an untyped bonus to weapon attack and
    damage rolls
  - these bonuses remain in effect until the target is dead or you study another
    target
  - if you deal sneak attack damage to an opponent, you can study the target as
    an immediate action
- [tactician](https://www.d20pfsrd.com/classes/core-classes/Fighter/archetypes/paizo-fighter-archetypes/drill-sergeant-fighter-archetype/#Tactician_Ex)  
  - you gain a bonus teamwork feat ([paired opportunist](https://www.d20pfsrd.com/feats/combat-feats/paired-opportunists-combat-teamwork/) was chosen)
  - once per day, as a standard action, you can grant this feat to all allies
    within 30 feat (that can see and hear you) for 4 rounds  
- [teamwork feats](https://www.d20pfsrd.com/classes/base-classes/Inquisitor/#TOC-teamwork-Feat)  
  a number of times per day equal to your wisdom bonus (3), as a standard
  action, you can choose to learn a new teamwork feat in place of the 
  teamwork feat you most recently learned. 
- [track](https://www.d20pfsrd.com/classes/base-classes/Inquisitor/#TOC-Track-Ex-)  
  you add half your inquisitor level (rounded down) to survival checks to follow
  or identify tracks

### spell casting

|          level | 0 | 1 |
|---------------:|:-:|:-:|
| spells per day | - | 4 |
|   spells known | 6 | 4 |
  
#### 0th level spells known
- [create water](https://www.d20pfsrd.com/magic/all-spells/c/create-water)
- [detect magic](https://www.d20pfsrd.com/magic/all-spells/d/detect-magic)
- [guidance](https://www.d20pfsrd.com/magic/all-spells/g/guidance)
- [read magic](https://www.d20pfsrd.com/magic/all-spells/r/read-magic)
- [sift](https://www.d20pfsrd.com/magic/all-spells/s/sift)
- [stabilize](https://www.d20pfsrd.com/magic/all-spells/s/stabilize)

#### 1st level spells known
- [bed of iron](https://www.d20pfsrd.com/magic/all-spells/b/bed-of-iron)
- [cure light wounds](https://www.d20pfsrd.com/magic/all-spells/c/cure-light-wounds)
- [heightened awareness](https://www.d20pfsrd.com/magic/all-spells/h/heightened-awareness)
- [know the enemy](https://www.d20pfsrd.com/magic/all-spells/k/know-the-enemy) 

## feats
- [combat reflexes](https://www.d20pfsrd.com/feats/combat-feats/combat-reflexes-combat/)
- [dirty fighting](https://www.d20pfsrd.com/feats/combat-feats/dirty-fighting-combat/)
- [improved trip](https://www.d20pfsrd.com/feats/combat-feats/improved-trip-combat/)
- [paired opportunist](https://www.d20pfsrd.com/feats/combat-feats/paired-opportunists-combat-teamwork/)
- [power attack](https://www.d20pfsrd.com/feats/combat-feats/power-attack/)
- [tandem trip](https://www.d20pfsrd.com/feats/combat-feats/tandem-trip-combat-teamwork/)
- [weapon trick: polearms](https://www.d20pfsrd.com/Feats/combat-feats/weapon-trick-combat/#Polearm_Tricks)

## equipment

|                                                                                           equipment                                                                                             | quantity |      value       |
|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------:|:--------:|-----------------:|
| +1 [cloak of resistance](https://www.d20pfsrd.com/magic-items/wondrous-items/c-d/cloak-of-resistance/)                                                                                          |     1    | 1000 gold pieces |
| [maiden helm](https://www.aonprd.com/MagicWondrousDisplay.aspx?FinalName=Maiden%27s%20Helm)                                                                                                     |     1    | 3500 gold pieces |
| +1 [dwarven giant-sticker](https://www.aonprd.com/EquipmentWeaponsDisplay.aspx?ItemName=Giant-sticker,%20dwarven)                                                                               |     1    | 2325 gold pieces |
| masterwork [cold iron](https://www.d20pfsrd.com/equipmenT/special-materials/#Iron_Cold) [sphinx hammmer](https://www.d20pfsrd.com/equipment/weapons/weapon-descriptions/sphinx-hammer-dwarven/) |     1    |  390 gold pieces |
| +1 [o-yoroi](https://www.aonprd.com/EquipmentArmorDisplay.aspx?ItemName=O-yoroi)                                                                                                                |     1    | 2850 gold pieces |
| +1 [ring of protection](https://www.d20pfsrd.com/magic-items/rings/ring-of-protection/)                                                                                                         |     1    | 2000 gold pieces |
| scroll of [returning weapon](https://www.d20pfsrd.com/magic/all-spells/r/returning-weapon) (caster level 2)                                                                                     |     1    |   50 gold pieces |
| wand of [cure light wounds](https://www.d20pfsrd.com/magic/all-spells/c/cure-light-wounds) (caster level 1, 25 charges)                                                                         |     1    |  225 gold pieces |



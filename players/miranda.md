# Marvelous Madam Mimoph

## classes

[swashbucker](https://www.d20pfsrd.com/classes/hybrid-classes/swashbuckler/) ([inspired blade](https://www.d20pfsrd.com/classes/hybrid-classes/swashbuckler/archetypes/paizo-swashbuckler-archetypes/inspired-blade)) 1  
[investigator](https://www.d20pfsrd.com/classes/hybrid-classes/investigator/) ([empiricist](https://www.d20pfsrd.com/classes/hybrid-classes/investigator/archetypes/paizo-investigator-archetypes/empiricist)) 4

**favored class:** investigator, swashbuckler

## race

half-elf
- **adaptability**
  - half-elves with this trait get **skill focus** as a bonus feat
- **drow heritage**
  - half-elves with this trait count as droaw for any effect related to race
- **elf blood**
  - half-elves with this trait count as both humans and elves for any effect
    related to race
- **keen senses**
  - +2 perception checks
- **low-light vision**
  - half-elves with this trait can see twice as far as humans in conditions of
    dim light
- **multitalented**
  - half-elves with this trait two favored classes
  - half-elves with this trait gain an extra hp or extra skill point for each
    level of a favored class
- **elven immunities**
  - half-elves with this trait are immune to magical *sleep* effects
  - half-elves with this trait gain a +2 racial saving throw bonus against
    enchantment spells and effects 

## traits

- [student of philosophy](https://www.d20pfsrd.com/traits/social-traits/student-of-philosophy/)
  - use your intelligence modifier in place of your charisma for **diplomacy**
    checks to persuade others
  - use your intelligence modifier in place of your charisma for **bluff**
    checks to convince others that a lie is true
- [blessed of the norns](https://www.aonprd.com/TraitDisplay.aspx?ItemName=Blessed%20of%20the%20Norns)  
  - +2 trait bonus on **perception** checks to notice traps and ambushes
  - +1 trait bonus to armor class against traps and on surprise rounds while unaware

## attributes
|              | **str** | **dex** | **con** | **int** | **wis** | **cha** |
|-------------:|:-------:|:-------:|:-------:|:-------:|:-------:|:-------:|
| **score**    |    7    |   20    |   14    |   16    |   10    |    7    |
| **modifier** |   -2    |   +5    |   +2    |   +3    |   +0    |   -2    |

## statistic

**hit points:** 39 = 28 (class) + 10 (constitution) + 1 (favored class)  
**initiative:** +5  
**base attack bonus:** +4
**combat maneuver defense:** 17 = 10 + 4 (base attack bonus) - 2 (strength) + 5 (dexterity)

### armor class

|          type         |   total   |                        components                          |
|----------------------:|:---------:|:-----------------------------------------------------------|
|          **standard** |     21    | 10 + 5 (armor) + 5 (dexterity) + 1 (deflection)            |
|             **touch** |     16    | 10 + 5 (dexterity)+ 1 (deflection)                         |
|       **flat-footed** |     16    | 10 + 5 (armor) + 1 (deflection)                            |
| **flat-footed touch** |     11    | 10 + 1 (deflection)                                        |

:::{tip}
- the **shield** 1st level investigator extract can provide a bonus (+4 shield)
  to standard armor class for a short duration (5 minutes)
- the **barkskin** 2st level investigator extract can provide a natural armor
  bonus (+2) to standard and flat-footed armor class for a moderate duration
  (50 minutes)
- the mutagen class feature can be used to provide a bonus to dexterity (+4
  alchemical) for a moderate duration (50 minutes), providing a +2 bonus to
  standard and touch armor class.
:::

### saving throws

|     type      |   total   |            components               |
|--------------:|:---------:|:------------------------------------|
| **fortitude** |     4     |  1 + 2 (attribute) + 1 (resistance) |
|    **reflex** |    12     |  6 + 5 (attribute) + 1 (resistance) |
|      **will** |     5     |  4 + 0 (attribute) + 1 (resistance) |

:::{note}
- **reflex**
  - the mutagen class feature can be used to provide a bonus to dexterity (+4
    alchemical) for a moderate duration (50 minutes), providing a +2 bonus to
    reflex
- **will**
  - +2 racial bonus versus enchantment effects 
:::

### attacks
#### +1 rapier

**damage dice:** 1d6  
**damage type:** piercing  
**critical range:** 18-20  
**critical multiplier:** x2  
**categories:** one-handed  
**weapon group:** light blades  
**special:** finesse   

:::{table} standard melee attack
:widths: grid

|              |  total  | components                                                                 |
|-------------:|--------:|:---------------------------------------------------------------------------|
| attack bonus |     +11 | 4 (base attack bonus) + 5 (dexterity) + 1 (enhancement) + 1 (weapon focus) |
|       damage | 1d6 + 6 | 5 (dexterity) + 1 (enhancement)                                            |

:::

:::{tip}
- The studied combat (+2) class feature can provide a bonus to attack and
  damage rolls against a studied target 
- the mutagen class feature can be used to provide a bonus to dexterity (+4
  alchemical) for a moderate duration (50 minutes), providing a +2 bonus to
  attack and damage rolls
- After rolling an attack but before the success or failure is announced, you
  may spend 2 points from your inspiration pool to add 1d6+1 to the roll  
:::

### skills

**total:** 48 = 28 (class) + 15 (intelligence) + 5 (favored class)

|                                                                                 type | total |                    components                                                     | class skill | untrained |
|-------------------------------------------------------------------------------------:|:-----:|:----------------------------------------------------------------------------------|:-----------:|:---------:|
|                 [acrobatics](https://www.aonprd.com/Skills.aspx?ItemName=Acrobatics) |       | 0 (ranks)                                                                                                   |     yes     |    yes    | 
|                     [appraise](https://www.aonprd.com/Skills.aspx?ItemName=Appraise) |       | 0 (ranks)                                                                                                   |     yes     |    yes    |
|                           [bluff](https://www.aonprd.com/Skills.aspx?ItemName=Bluff) |  +11  | 1 (ranks) + 3 (intelligence) + 3 (class skill) + 4 (fey obedience)                                          |     yes     |    yes    |
|                           [climb](https://www.aonprd.com/Skills.aspx?ItemName=Climb) |       | 0 (ranks)                                                                                                   |     yes     |    yes    |
|                           [craft](https://www.aonprd.com/Skills.aspx?ItemName=Craft) |       | 0 (ranks)                                                                                                   |     yes     |    yes    |
|                 [diplomacy](https://www.aonprd.com/Skills.aspx?ItemName=Diplomacy)\* |  +11  | 1 (ranks) + 3 (intelligence) + 3 (class skill) + 4 (fey obedience)                                          |     yes     |    yes    |
|       [disable device](https://www.aonprd.com/Skills.aspx?ItemName=Disable%20Device) |  +19  | 5 (ranks) + 3 (intelligence) + 3 (class skill) + 4 (fey obedience) + 2 (trapfinding) + 2 (competence)       |     yes     |     no    |
|                     [disguise](https://www.aonprd.com/Skills.aspx?ItemName=Disguise) |       | 0 (ranks)                                                                                                   |     yes     |    yes    |
|         [escape artist](https://www.aonprd.com/Skills.aspx?ItemName=Escape%20Artist) |       | 0 (ranks)                                                                                                   |     yes     |    yes    |
|                               [fly](https://www.aonprd.com/Skills.aspx?ItemName=Fly) |       | 0 (ranks)                                                                                                   |      no     |    yes    |
|         [handle animal](https://www.aonprd.com/Skills.aspx?ItemName=Handle%20Animal) |       | 0 (ranks)                                                                                                   |      no     |     no    |
|                             [heal](https://www.aonprd.com/Skills.aspx?ItemName=Heal) |       | 0 (ranks)                                                                                                   |     yes     |    yes    |
|                 [intimidate](https://www.aonprd.com/Skills.aspx?ItemName=Intimidate) |       | 0 (ranks)                                                                                                   |     yes     |    yes    |
|        [knowledge (arcana)](https://www.aonprd.com/Skills.aspx?ItemName=Knowledge)\* |  +11  | 1 (ranks) + 3 (intelligence) + 3 (class skill) + 4 (fey obedience)                                          |     yes     |     no    |
| [knowledge (dungeoneering)](https://www.aonprd.com/Skills.aspx?ItemName=Knowledge)\* |  +15  | 5 (ranks) + 3 (intelligence) + 3 (class skill) + 4 (fey obedience)                                          |     yes     |     no    |
|   [knowledge (engineering)](https://www.aonprd.com/Skills.aspx?ItemName=Knowledge)\* |  +11  | 1 (ranks) + 3 (intelligence) + 3 (class skill) + 4 (fey obedience)                                          |     yes     |     no    |
|     [knowledge (geography)](https://www.aonprd.com/Skills.aspx?ItemName=Knowledge)\* |  +11  | 1 (ranks) + 3 (intelligence) + 3 (class skill) + 4 (fey obedience)                                          |     yes     |     no    |
|       [knowledge (history)](https://www.aonprd.com/Skills.aspx?ItemName=Knowledge)\* |  +11  | 1 (ranks) + 3 (intelligence) + 3 (class skill) + 4 (fey obedience)                                          |     yes     |     no    |
|         [knowledge (local)](https://www.aonprd.com/Skills.aspx?ItemName=Knowledge)\* |  +11  | 1 (ranks) + 3 (intelligence) + 3 (class skill) + 4 (fey obedience)                                          |     yes     |     no    |
|        [knowledge (nature)](https://www.aonprd.com/Skills.aspx?ItemName=Knowledge)\* |  +11  | 1 (ranks) + 3 (intelligence) + 3 (class skill) + 4 (fey obedience)                                          |     yes     |     no    |
|      [knowledge (nobility)](https://www.aonprd.com/Skills.aspx?ItemName=Knowledge)\* |  +11  | 1 (ranks) + 3 (intelligence) + 3 (class skill) + 4 (fey obedience)                                          |     yes     |     no    |
|         [knowledge (plane)](https://www.aonprd.com/Skills.aspx?ItemName=Knowledge)\* |  +15  | 5 (ranks) + 3 (intelligence) + 3 (class skill) + 4 (fey obedience)                                          |     yes     |     no    |
|      [knowledge (religion)](https://www.aonprd.com/Skills.aspx?ItemName=Knowledge)\* |  +15  | 5 (ranks) + 3 (intelligence) + 3 (class skill) + 4 (fey obedience)                                          |     yes     |     no    |
|               [linguistics](https://www.aonprd.com/Skills.aspx?ItemName=Linguistics) |       |                                                                                                             |     yes     |     no    |
|               [perception](https://www.aonprd.com/Skills.aspx?ItemName=Perception)\* |  +19  | 5 (ranks) + 3 (intelligence) + 3 (class skill) + 4 (fey obedience) + 2 (racial) + 2 (competence)            |     yes     |    yes    |
|                       [perform](https://www.aonprd.com/Skills.aspx?ItemName=Perform) |       | 0 (ranks)                                                                                                   |     yes     |    yes    |
|                 [profession](https://www.aonprd.com/Skills.aspx?ItemName=Profession) |       | 0 (ranks)                                                                                                   |     yes     |     no    |
|                             [ride](https://www.aonprd.com/Skills.aspx?ItemName=Ride) |       | 0 (ranks)                                                                                                   |      no     |    yes    |
|         [sense motive](https://www.aonprd.com/Skills.aspx?ItemName=Sense%20Motive)\* |  +12  | 2 (ranks) + 3 (intelligence) + 3 (class skill) + 4 (fey obedience)                                          |     yes     |    yes    |
|   [sleight of hand](https://www.aonprd.com/Skills.aspx?ItemName=Sleight%20of%20Hand) |       | 0 (ranks)                                                                                                   |     yes     |     no    |
|               [spellcraft](https://www.aonprd.com/Skills.aspx?ItemName=Spellcraft)\* |  +13  | 3 (ranks) + 3 (intelligence) + 3 (class skill) + 4 (fey obedience)                                          |     yes     |    yes    |
|                       [stealth](https://www.aonprd.com/Skills.aspx?ItemName=Stealth) |  +16  | 5 (ranks) + 5 (dexterity) + 3 (class skill) + 3 (skill focus)                                               |     yes     |    yes    |
|                     [survival](https://www.aonprd.com/Skills.aspx?ItemName=Survival) |       | 0 (ranks) + 0 (widsom)                                                                                      |      no     |    yes    |
|                             [swim](https://www.aonprd.com/Skills.aspx?ItemName=Swim) |       | 0 (ranks)                                                                                                   |      no     |    yes    |
| [use magic device](https://www.aonprd.com/Skills.aspx?ItemName=Use%20Magic%20Device) |  +15  | 5 (ranks) + 3 (intelligence) + 3 (class skill) + 4 (fey obedience)                                          |     yes     |     no    |

:::{note}
- **perception**
  - +4 versus traps (trapfinding, blessed of the norms)
  - +2 to notice ambushes (blessed of the norms)
- Currently the floating competence bonus of the cracked magenta prism iuon
  stone is applied to perception
:::

:::{tip}
- The character may use the investigator's **inspiration** class feature with
  skills denoted with an asterisk (\*) without expending inspiration points.
- Taking into consideration the investigator'd **inspiration** class feature,
  the character will notice magical traps with following probabilities:
 
  | **spell level** |   1st   |   2nd   |   3rd   |   4th   |   5th   |   6th   |  7th   |  8th   |   9th   | 
  |:---------------:|:-------:|:-------:|:-------:|:-------:|:-------:|:-------:|:------:|:------:|:-------:|
  | **confidence**  |  99.27  |  97.50  |  95.00  |  91.67  |  87.50  |  82.50  |  77.50 |  72.50 |  67.50  |

- **perceive cues** is a 2nd level extract with a moderately long duration (40
  minutes) which increases the likelihood of noticing traps by 25%.

- **blend** is a 1st level extract with a moderately long duration (40 minutes)
  that provides a significant bonus (+4) to stealth check and allows for stealth
  checks even without cover or concealment while moving at half speed.
 
:::

## class features

- [alchemy](https://www.d20pfsrd.com/classes/Hybrid-Classes/Investigator/#Alchemy_Su)
- [ceaseless observation](https://www.d20pfsrd.com/classes/hybrid-classes/Investigator/archetypes/paizo-investigator-archetypes/empiricist/#Ceaseless_Observation_Ex)
- [deeds](https://www.d20pfsrd.com/classes/Hybrid-Classes/Swashbuckler/#Deeds)
  - opportune parry and riposte
- [deeds of renown](https://www.d20pfsrd.com/classes/Hybrid-Classes/Swashbuckler/#Deeds_of_Renown)
  - vengeful heart ([resolve](https://www.d20pfsrd.com/classes/Alternate-Classes/samurai/#TOC-Resolve-Ex-))
- [inspiration](https://www.d20pfsrd.com/classes/Hybrid-Classes/Investigator/#Inspiration_Ex) 5/day (1d6 + 1)
- [inspired finesse](https://www.d20pfsrd.com/classes/hybrid-classes/swashbuckler/archetypes/paizo-swashbuckler-archetypes/inspired-blade#TOC-Inspired-Finesse-Ex-)
- [inspired panache](https://www.d20pfsrd.com/classes/hybrid-classes/swashbuckler/archetypes/paizo-swashbuckler-archetypes/inspired-blade#TOC-Inspired-Panache-Ex-)
- [trap sense](https://www.d20pfsrd.com/classes/Hybrid-Classes/Investigator/#Trap_Sense_Ex) (+1)
- [trapfinding](https://www.d20pfsrd.com/classes/Hybrid-Classes/Investigator/#Trapfinding) (+2)
- [investigator talent](https://www.d20pfsrd.com/classes/Hybrid-Classes/Investigator/#Investigator_Talent_Ex_or_Su)
  - [alchemist discovery](https://www.d20pfsrd.com/classes/hybrid-classes/investigator/investigator-talents/paizo-investigator-talents/alchemist-discovery): [mutagen](https://www.d20pfsrd.com/classes/base-classes/alchemist/discoveries/paizo-alchemist-discoveries/mutagen-su)
  - [expanded inspiration](https://www.d20pfsrd.com/classes/hybrid-classes/investigator/investigator-talents/paizo-investigator-talents/expanded-inspiration)
- [keen recollection](https://www.d20pfsrd.com/classes/Hybrid-Classes/Investigator/#Keen_Recollection)
- [panache](https://www.d20pfsrd.com/classes/Hybrid-Classes/Swashbuckler/#Panache_Ex) (4 points)
- [studied combat](https://www.d20pfsrd.com/classes/Hybrid-Classes/Investigator/#Studied_Combat_Ex) (+2)
- [studied strike](https://www.d20pfsrd.com/classes/Hybrid-Classes/Investigator/#Studied_Strike_Ex) (+1d6)
- [unfailing logic](https://www.d20pfsrd.com/classes/hybrid-classes/Investigator/archetypes/paizo-investigator-archetypes/empiricist/#Unfailing_Logic_Ex)

:::{note}
- The half-elf favored class bonus for investigator has been selected four times, providing a +1 bonus to inspiration rolls
:::

## feats

- [extra investigator talent](https://www.d20pfsrd.com/feats/general-feats/extra-investigator-talent/): [expanded inspiration](https://www.d20pfsrd.com/classes/hybrid-classes/investigator/investigator-talents/paizo-investigator-talents/expanded-inspiration)
- [fey obedience](https://aonprd.com/FeatDisplay.aspx?ItemName=Fey%20Obedience): [magdh](https://www.aonprd.com/DeityDisplay.aspx?ItemName=Magdh)
- [fencing grace](https://www.aonprd.com/FeatDisplay.aspx?ItemName=Fencing%20Grace)
- [skill focus](https://www.d20pfsrd.com/feats/general-feats/skill-focus/): stealth
- [weapon focus](https://www.aonprd.com/FeatDisplay.aspx?ItemName=Weapon%20Focus): rapier
- [weapon finesse](https://www.aonprd.com/FeatDisplay.aspx?ItemName=Weapon%20Finesse)

### alchemy

|            level | 1 | 2 |
|-----------------:|:-:|:-:|
| extracts per day | 4 | 2 |

#### 1st level formula known
- [blend](https://www.aonprd.com/SpellDisplay.aspx?ItemName=Blend)
- [comprehend languages](https://www.d20pfsrd.com/magic/all-spells/c/comprehend-languages)
- [cure light wounds](https://www.aonprd.com/SpellDisplay.aspx?ItemName=Cure%20Light%20Wounds)
- [disguise self](https://www.d20pfsrd.com/magic/all-spells/d/disguise-self)
- [negate aroma](https://www.d20pfsrd.com/magic/all-spells/n/negate-aroma/) \*
- [reduce person](https://www.aonprd.com/SpellDisplay.aspx?ItemName=Reduce%20Person)
- [shield](https://www.d20pfsrd.com/magic/all-spells/s/shield)
- [tears to wine](https://www.d20pfsrd.com/magic/all-spells/t/tears-to-wine)

#### 2nd level formula known
- [barkskin](https://www.d20pfsrd.com/magic/all-spells/b/barkskin)
- [darkvision](https://www.aonprd.com/SpellDisplay.aspx?ItemName=Darkvision)\*
- [perceive cues](https://www.aonprd.com/SpellDisplay.aspx?ItemName=Perceive%20Cues)\*

:::{note}

The spells denoted with a asterisk were added to the formulae book by referencing
the resources of other alchemists or wizards.
:::

:::{tip}

**tears to wine** is amazing for this character
:::

## equipment

|                                                                                           equipment                                                                                             | quantity |      value       |
|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------:|:--------:|-----------------:|
| ([access to spell/formulae book](https://aonprd.com/Rules.aspx?Name=Arcane%20Magical%20Writing&Category=Arcane%20Spells))                                                                       | -        |  135 gold pieces |
| [boots of the soft step](https://www.d20pfsrd.com/magic-items/wondrous-items/a-b/boots-of-the-soft-step/)                                                                                       |     1    | 1000 gold pieces |
| +1 [cloak of resistance](https://www.d20pfsrd.com/magic-items/wondrous-items/c-d/cloak-of-resistance/)                                                                                          |     1    | 1000 gold pieces |
| [ioun stone](https://www.d20pfsrd.com/magic-items/wondrous-items/h-l/ioun-stones/) ([cracked magenta prism](https://www.d20pfsrd.com/magic-items/wondrous-items/h-l/ioun-stones/magenta-prism))                                                                                      |     1    |  800 gold pieces |
| +1 [mithral chainshirt](https://www.d20pfsrd.com/magic-items/magic-armor/specific-magic-armor/mithral-shirt/)                                                                                   |     1    | 2100 gold pieces |
| +1 rapier                                                                                                                                                                                       |     1    | 2350 gold pieces |
| +1 [ring of protection](https://www.d20pfsrd.com/magic-items/rings/ring-of-protection/)                                                                                                         |     1    | 2000 gold pieces |
| [scent blocker](https://www.aonprd.com/EquipmentMiscDisplay.aspx?ItemName=Scent%20blocker)                                                                                                      |     3    |  240 gold pieces |
| [thieves' tools, masterwork](https://www.aonprd.com/EquipmentMiscDisplay.aspx?ItemName=Thieves%27%20tools%20(masterwork))                                                                                                      |     1    |  100 gold pieces |
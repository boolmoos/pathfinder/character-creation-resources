## classes  

[alchemist](https://www.d20pfsrd.com/Classes/base-Classes/alchemist/) ([saboteur](https://www.d20pfsrd.com/races/core-races/gnome/saboteur-alchemist-gnome/), [grenadier](https://www.d20pfsrd.com/classes/base-classes/alchemist/archetypes/paizo-alchemist-archetypes/grenadier)) 5

## race

gnome
- small
- **keen senses**
  - +2 racial bonus on perception
- **low light vision**
  - This character can see twice as far as humans in conditions of dim light.
- **obsessive**
  - +2 racial bonus on **craft (alchemy)**.
- **pyromaniac**
  - This character is considered one level higher for the purposes of
    calculating damage with alchemist bombs dealing fire damage
- **warden of nature**
  - +2 ac vs aberrations, oozes, and vermin
  - +1 to attacks vs aberrations, oozes, and vermin

## traits

- [excitable](https://www.aonprd.com/TraitDisplay.aspx?ItemName=Excitable)
  - +2 racial bonus to initiative
- [slippery](https://www.aonprd.com/TraitDisplay.aspx?ItemName=Slippery)
  - +1 trait bonus to stealth and stealth is a class skill for you

## attributes

|              | **str** | **dex** | **con** | **int** | **wis** | **cha** |
|-------------:|:-------:|:-------:|:-------:|:-------:|:-------:|:-------:|
| **score**    |    7    |   14    |   16    |   18    |   12    |    9    |
| **modifier** |   -2    |   +2    |   +3    |   +4    |   +1    |   -1    |

## statistics

**hit points:**  41 = 26 (class) + 15 (constitution)  
**initiative:** +4 = +2 (dexterity) + 2 (excitable)  
**base attack bonus:** +3  
**combat maneuver defense:** 12 = 10 + 3 (base attack bonus) - 2 (strength) + 2 (dexterity) - 1 (size)

### armor class

|          type         |   total   |                        components                          |
|----------------------:|:---------:|:-----------------------------------------------------------|
|          **standard** |     19    | 10 + 5 (armor) + 2 (dexterity) + 1 (size) + 1 (deflection) |
|             **touch** |     14    | 10 + 2 (dexterity) + 1 (size) + 1 (deflection)             |
|       **flat-footed** |     17    | 10 + 5 (armor) + 1 (size) + 1 (deflection)                 |
| **flat-footed touch** |     12    | 10 + 1 (size) + 1 (deflection)                             |

:::{note}
- +2 vs aberrations
- +2 vs oozes
- +2 vs vermin
:::

:::{tip}
- the **shield** 1st level alchemist extract can provide a bonus (+4 shield)
  to standard armor class for a short duration (5 minutes).
- the **barkskin** 2nd level alchemist extract can provide a natural armor
  bonus (+2) to standard and flat-footed armor class for a moderate duration
  (50 minutes).
- the **cat's grace** 2nd level alchemist extract can provide a significant
  bonus (+4 enhancement) to dexterity for a short suration (5 minutes),
  improving standard and touch armor class by 2.
:::

### saving throws

|     type      |   total   |            components               |
|--------------:|:---------:|:------------------------------------|
| **fortitude** |     8     |  4 + 3 (attribute) + 1 (resistance) |
|    **reflex** |     7     |  4 + 2 (attribute) + 1 (resistance) |
|      **will** |     3     |  1 + 1 (attribute) + 1 (resistance) |

### attacks
#### masterwork longbow

**damage dice:** 1d6  
**damage type:** piercing  
**critical range:** 20  
**critical multiplier:** x3  
**range increment:** 100 ft (projectile)
**weapon group:** ranged

:::{table} standard ranged attack (painted arrow)
:widths: grid

|              |  total  | components                                                                 |
|-------------:|--------:|:---------------------------------------------------------------------------|
| attack bonus |      +7 | 3 (base attack bonus) + 2 (dexterity) + 1 (enhancement) + 1 (size)         |
|       damage |     N/A |                                                                            |

:::

:::{table} standard ranged attack (tangleshot arrow)
:widths: grid

|              |  total  | components                                                                 |
|-------------:|--------:|:---------------------------------------------------------------------------|
| attack bonus |      +7 | 3 (base attack bonus) + 2 (dexterity) + 1 (enhancement) + 1 (size)         |
|       damage |     N/A |                                                                            |

:::

:::{note}
- The **tangleshot arrow** imposes the entangled condition on creates of large
  size or smaller on hit, similar to the tangelfoot bag. See the item
  description for more information. 
:::

:::{tip}
- The **explosive missile** alchemist discovery can be used to deliver bombs with
  a ranged attack with the bow as a standard action
- The **alchemical weapon** class feature of the grenadier alchemist archetype can
  infuse an alchemical items (e.g., alchemist fire, ghast retch, or accelerant)
  a piece of ammunition (e.g., a painted arrow or entangling arrow) as a move
  action.
:::

#### bomb

**damage dice:** 3d6  
**damage type:** fire  
**critical range:** 20  
**critical multiplier:** x2  

:::{table} standard action attack
:widths: grid

|              |  total  | components                                                                 |
|-------------:|--------:|:---------------------------------------------------------------------------|
| attack bonus |      +7 | 3 (base attack bonus) + 2 (dexterity) + 1 (throw anything) + 1 (size)      |
|       damage | 3d6 + 4 | 4 (intelligence)                                                           |

:::

:::{note}
- Upon impact, an alchemist bomb splashes to adjacent squares. Creatures in
  these squares are subject to the minimum bomb damage (7 = 3 + 4). A
  successful reflex save (DC 16 = 10 + 2 + 4) reduces this damage by half.
- The **precise bomb** alchemist discovery allows the alchemist to exclude a
  number of squares up to her intelligence modifier (4) from splash damage.
- When preparing an alchemist bomb, the alchemist discovery **sand bomb**
  allows the alchemist to modify the bomb such that the target of a successful
  attack is blinded for one round. Moreover, creatures within the splash radius
  (5 ft) of a chosen corner of the target's square are also blinded unless they
  succeed in reflex save (DC 16 = 10 + 2 + 4).  
:::

### skills

**total:** 40 = 20 (class) + 20 (intelligence)

|                                                                                 type | total |                    components                                                     | class skill | untrained |
|-------------------------------------------------------------------------------------:|:-----:|:----------------------------------------------------------------------------------|:-----------:|:---------:|
|                 [acrobatics](https://www.aonprd.com/Skills.aspx?ItemName=Acrobatics) |   +2  | 0 (ranks) + 2 (dexterity)                                                         |      no     |    yes    | 
|                     [appraise](https://www.aonprd.com/Skills.aspx?ItemName=Appraise) |   +8  | 1 (ranks) + 4 (intelligence) + 3 (class skill)                                    |     yes     |    yes    |
|                           [bluff](https://www.aonprd.com/Skills.aspx?ItemName=Bluff) |   +0  | 1 (ranks) - 1 (charisma)                                                          |      no     |    yes    |
|                           [climb](https://www.aonprd.com/Skills.aspx?ItemName=Climb) |   -2  | 0 (ranks) - 2 (strength)                                                          |      no     |    yes    |
|                  [craft](https://www.aonprd.com/Skills.aspx?ItemName=Craft): alchemy |  +19  | 5 (ranks) + 4 (intelligence) + 3 (class skill) + 2 (obsessive) + 5 (competence)   |     yes     |    yes    |
|                   [diplomacy](https://www.aonprd.com/Skills.aspx?ItemName=Diplomacy) |   -1  | 0 (ranks) - 1 (charisma)                                                          |      no     |    yes    |
|       [disable device](https://www.aonprd.com/Skills.aspx?ItemName=Disable%20Device) |  +12  | 5 (ranks) + 2 (dexterity) + 3 (class skill) + 2 (competence)                      |     yes     |     no    |
|                     [disguise](https://www.aonprd.com/Skills.aspx?ItemName=Disguise) |   -1  | 0 (ranks) - 1 (charisma)                                                          |      no     |    yes    |
|         [escape artist](https://www.aonprd.com/Skills.aspx?ItemName=Escape%20Artist) |    2  | 0 (ranks) + 2 (dexterity)                                                         |      no     |    yes    |
|                               [fly](https://www.aonprd.com/Skills.aspx?ItemName=Fly) |    2  | 0 (ranks) + 2 (dexterity)                                                         |     yes     |    yes    |
|         [handle animal](https://www.aonprd.com/Skills.aspx?ItemName=Handle%20Animal) |  n/a  | 0 (ranks) - 1 (charisma)                                                          |      no     |     no    |
|                             [heal](https://www.aonprd.com/Skills.aspx?ItemName=Heal) |   +7  | 3 (ranks) + 1 (wisdom) + 3 (class skill)                                          |     yes     |    yes    |
|                 [intimidate](https://www.aonprd.com/Skills.aspx?ItemName=Intimidate) |   -1  | 0 (ranks) - 1 (charisma)                                                          |      no     |    yes    |
|          [knowledge (arcana)](https://www.aonprd.com/Skills.aspx?ItemName=Knowledge) |  +12  | 5 (ranks) + 4 (intelligence) + 3 (class skill)                                    |     yes     |     no    |
|   [knowledge (dungeoneering)](https://www.aonprd.com/Skills.aspx?ItemName=Knowledge) |   +8  | 1 (ranks) + 4 (intelligence) + 3 (class skill)                                    |     yes     |     no    |
|     [knowledge (engineering)](https://www.aonprd.com/Skills.aspx?ItemName=Knowledge) |  +11  | 4 (ranks) + 4 (intelligence) + 3 (class skill)                                    |     yes     |     no    |
|       [knowledge (geography)](https://www.aonprd.com/Skills.aspx?ItemName=Knowledge) |  n/a  | 0 (ranks) + 4 (intelligence)                                                      |      no     |     no    |
|         [knowledge (history)](https://www.aonprd.com/Skills.aspx?ItemName=Knowledge) |  n/a  | 0 (ranks) + 4 (intelligence)                                                      |      no     |     no    |
|           [knowledge (local)](https://www.aonprd.com/Skills.aspx?ItemName=Knowledge) |  n/a  | 0 (ranks) + 4 (intelligence)                                                      |      no     |     no    |
|          [knowledge (nature)](https://www.aonprd.com/Skills.aspx?ItemName=Knowledge) |  n/a  | 0 (ranks) + 4 (intelligence)                                                      |      no     |     no    |
|        [knowledge (nobility)](https://www.aonprd.com/Skills.aspx?ItemName=Knowledge) |  n/a  | 0 (ranks) + 4 (intelligence)                                                      |      no     |     no    |
|           [knowledge (plane)](https://www.aonprd.com/Skills.aspx?ItemName=Knowledge) |  n/a  | 0 (ranks) + 4 (intelligence)                                                      |      no     |     no    |
|        [knowledge (religion)](https://www.aonprd.com/Skills.aspx?ItemName=Knowledge) |  n/a  | 0 (ranks) + 4 (intelligence)                                                      |      no     |     no    |
|               [linguistics](https://www.aonprd.com/Skills.aspx?ItemName=Linguistics) |  n/a  | 0 (ranks) + 4 (intelligence)                                                      |      no     |     no    |
|                 [perception](https://www.aonprd.com/Skills.aspx?ItemName=Perception) |   11  | 5 (ranks) + 1 (wisdom) + 3 (class skill) + 2 (racial)                             |     yes     |    yes    |
|                       [perform](https://www.aonprd.com/Skills.aspx?ItemName=Perform) |   -1  | 0 (ranks) - 1 (charisma)                                                          |      no     |    yes    |
|                 [profession](https://www.aonprd.com/Skills.aspx?ItemName=Profession) |  n/a  | 0 (ranks) + 1 (wisdom)                                                            |     yes     |     no    |
|                             [ride](https://www.aonprd.com/Skills.aspx?ItemName=Ride) |    2  | 0 (ranks) + 2 (dexterity)                                                         |      no     |    yes    |
|           [sense motive](https://www.aonprd.com/Skills.aspx?ItemName=Sense%20Motive) |    1  | 0 (ranks) + 1 (wisdom)                                                            |      no     |    yes    |
|   [sleight of hand](https://www.aonprd.com/Skills.aspx?ItemName=Sleight%20of%20Hand) |  n/a  | 0 (ranks) + 2 (dexterity)                                                         |     yes     |     no    |
|                 [spellcraft](https://www.aonprd.com/Skills.aspx?ItemName=Spellcraft) |  +12  | 5 (ranks) + 4 (intelligence) + 3 (class skill)                                    |     yes     |     no    |
|                       [stealth](https://www.aonprd.com/Skills.aspx?ItemName=Stealth) |  +15  | 5 (ranks) + 2 (dexterity) + 3 (class skill) + 4 (size) + 1 (trait)                |     yes     |    yes    |
|                     [survival](https://www.aonprd.com/Skills.aspx?ItemName=Survival) |    1  | 0 (ranks) + 1 (wisdom)                                                            |     yes     |    yes    |
|                             [swim](https://www.aonprd.com/Skills.aspx?ItemName=Swim) |   -2  | 0 (ranks) - 2 (strength)                                                          |      no     |    yes    |
| [use magic device](https://www.aonprd.com/Skills.aspx?ItemName=Use%20Magic%20Device) |   -1  | 0 (ranks) - 1 (charisma)                                                          |     yes     |     no    |

:::{tip}
- **diplomacy**
  - the **focused scrutiny** 2nd level alchemist extract provides a large bonus
    (+5) for moderate duration (50 minutes) for checks related to a particular
    creature.
- **intimidate**
  - the **focused scrutiny** 2nd level alchemist extract provides a large bonus
    (+5) for moderate duration (50 minutes) for checks related to a particular
    creature.
- **knowledge**
  - the **investigative mind** 2nd level alchemist extract provides advantage
    (i.e., roll twice and take the better value) for this skill for a moderate
    duration (50 minutes) for a number uses equal to your alchemist level.
  - the **heightened awareness** 1st level alchemist extract provides a bonus
    (+2 competence) for moderate duration (50 minutes) for skills in which the
    recipient has at least 1 rank.
  - the alcohol created by the **tears to wine** provides a bonus
    (+2 enhancement) for a moderate duration (50 minutes)
- **perception**
  - the **acute senses** 2nd level alchemist extract provides a large bonus
    (+10 enhancement) for short duration (5 minutes).
  - the **focused scrutiny** 2nd level alchemist extract provides a large bonus
    (+10) for moderate duration (50 minutes) for checks related to a particular
    creature.
  - the **heightened awareness** 1st level alchemist extract provides a bonus
    (+2 competence) for moderate duration (50 minutes).
  - the **perceive cues** 2nd level alchemist extract provides a large bonus
    (+5 competence) for moderate duration (50 minutes).
  - the alcohol created by the **tears to wine** provides a bonus
    (+2 enhancement) for a moderate duration (50 minutes)
- **sense motive**
  - the **focused scrutiny** 2nd level alchemist extract provides a large bonus
    (+10) for moderate duration (50 minutes) for checks related to a particular
    creature.
  - the **perceive cues** 2nd level alchemist extract provides a large bonus
    (+5 competence) for moderate duration (50 minutes.
  - the alcohol created by the **tears to wine** provides a bonus
    (+2 enhancement) for a moderate duration (50 minutes)
- **spellcraft**
  - the **investigative mind** 2nd level alchemist extract provides advantage
    (i.e., roll twice and take the better value) for this skill for a moderate
    duration (50 minutes) for a number uses equal to your alchemist level.
  - the alcohol created by the **tears to wine** provides a bonus
    (+2 enhancement) for a moderate duration (50 minutes)
- **stealth**
  - the **chameleon mutagen** class feature of the **saboteur** alchemist
    archetype provides a bonus (+2) with moderate duration (50 minutes) and a
    climb speed when imbided.
  - the **invisibility** 2nd level alchemist extract provides an overwhelming
    bonus (+40 when still, +20 when moving) with short duration (5
    minutes).
- **survival**
  - the **focused scrutiny** 2nd level alchemist extract provides a large bonus
    (+10) for moderate duration (50 minutes) for checks related to a particular
    creature.
  - the alcohol created by the **tears to wine** provides a bonus
    (+2 enhancement) for a moderate duration (50 minutes)
:::

## class features
 
- [alchemical weapon](https://www.d20pfsrd.com/classes/base-classes/alchemist/archetypes/paizo-alchemist-archetypes/grenadier/#TOC-Alchemical-Weapon-SU)
- [alchemy](https://www.d20pfsrd.com/classes/base-classes/Alchemist/#TOC-Alchemy-Su-)
- [bomb](https://www.d20pfsrd.com/classes/base-classes/Alchemist/#TOC-Bomb-Su-) 3d6 (11/day)
- [bonus discovery](https://www.d20pfsrd.com/classes/base-classes/alchemist/archetypes/paizo-alchemist-archetypes/grenadier/#TOC-Precise-bombs-EX) ([precise bombs](https://www.d20pfsrd.com/classes/base-classes/alchemist/discoveries/paizo-alchemist-discoveries/precise-bombs/))
- [chameleon mutagen](https://www.d20pfsrd.com/races/core-races/gnome/saboteur-alchemist-gnome/#Chameleon_Mutagen_Su)
- [discovery](https://www.d20pfsrd.com/classes/base-classes/Alchemist/#TOC-Discovery-Su-)
  - [infusion](https://www.d20pfsrd.com/classes/base-classes/alchemist/discoveries/paizo-alchemist-discoveries/infusion)
  - [explosive missile](https://www.d20pfsrd.com/classes/base-classes/alchemist/discoveries/paizo-alchemist-discoveries/explosive-missile)
  - [precise bombs](https://www.d20pfsrd.com/classes/base-classes/alchemist/discoveries/paizo-alchemist-discoveries/precise-bombs/)
  - [sand bomb](https://www.aonprd.com/AlchemistDiscoveries.aspx)
- [saboteur discoveries](https://www.d20pfsrd.com/races/core-races/gnome/saboteur-alchemist-gnome/#Saboteur_Discoveries)
- [swift alchemy](https://www.d20pfsrd.com/classes/base-classes/Alchemist/#TOC-Swift-Alchemy-Ex-)
- [throw anything](https://www.d20pfsrd.com/classes/base-classes/Alchemist/#TOC-Throw-Anything-Ex-)

## feats

- [extra discovery](https://www.d20pfsrd.com/feats/general-feats/extra-discovery/) ([infusion](https://www.d20pfsrd.com/classes/base-classes/alchemist/discoveries/paizo-alchemist-discoveries/infusion))
- [precise shot](https://www.d20pfsrd.com/feats/combat-feats/precise-shot-combat/)
- [point-blank shot](https://www.d20pfsrd.com/feats/combat-feats/point-blank-shot-combat/)

### alchemy

|            level | 1 | 2 |
|-----------------:|:-:|:-:|
| extracts per day | 5 | 3 |


#### 1st level formula known
- [ant haul](https://www.d20pfsrd.com/magic/all-spells/a/ant-haul)
- [crafter's fortune](https://www.d20pfsrd.com/magic/all-spells/c/crafter-s-fortune)
- [cure light wounds](https://www.d20pfsrd.com/magic/all-spells/c/cure-light-wounds)
- [enlarge person](https://www.d20pfsrd.com/magic/all-spells/e/enlarge-person)
- [heightened awareness](https://www.d20pfsrd.com/magic/all-spells/h/heightened-awareness)
- [identify](https://www.d20pfsrd.com/magic/all-spells/i/identify)
- [long arm](https://www.d20pfsrd.com/magic/all-spells/l/long-arm)
- [shield](https://www.d20pfsrd.com/magic/all-spells/s/shield)\*
- [targeted bomb admixture](https://www.d20pfsrd.com/magic/all-spells/t/targeted-bomb-admixture)\*
- [tears to wine](https://www.d20pfsrd.com/magic/all-spells/t/tears-to-wine)\*
- [true strike](https://www.d20pfsrd.com/magic/all-spells/t/true-strike)\*

#### 2nd level formula known
- [adhesive blood](https://www.d20pfsrd.com/magic/all-spells/a/adhesive-blood)
- [acute senses](https://www.d20pfsrd.com/magic/all-spells/a/acute-senses)
- [alchemical allocation](https://www.d20pfsrd.com/magic/all-spells/a/alchemical-allocation)\*
- [barkskin](https://www.d20pfsrd.com/magic/all-spells/b/barkskin)\*
- [cat's grace](https://www.d20pfsrd.com/magic/all-spells/c/cat-s-grace/)\*
- [darkvision](https://www.d20pfsrd.com/magic/all-spells/d/darkvision)\*
- [delay poison](https://www.d20pfsrd.com/magic/all-spells/d/delay-poison)\*
- [fox's cunning](https://www.d20pfsrd.com/magic/all-spells/f/fox-s-cunning)\*
- [focused scrutiny](https://www.d20pfsrd.com/magic/all-spells/f/focused-scrutiny)\*
- [full pouch](https://www.d20pfsrd.com/magic/all-spells/f/full-pouch)\*
- [investigative mind](https://www.d20pfsrd.com/magic/all-spells/i/investigative-mind)\*
- [invisibility](https://www.d20pfsrd.com/magic/all-spells/i/invisibility)\*
- [perceive cues](https://www.d20pfsrd.com/magic/all-spells/p/perceive-cues)\*
- [restoration](https://www.d20pfsrd.com/magic/all-spells/r/restoration)\*
- [resist energy](https://www.d20pfsrd.com/magic/all-spells/r/resist-energy)\*

## equipment

|                                                                                           equipment                                                                                             | quantity |      value       |
|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------:|:--------:|-----------------:|
| +1 [cloak of resistance](https://www.d20pfsrd.com/magic-items/wondrous-items/c-d/cloak-of-resistance/)                                                                                          |     1    | 1000 gold pieces |
| +1 [darkleaf](https://www.d20pfsrd.com/equipmenT/special-materials/#Darkleaf_Cloth) [lamellar leather](https://www.aonprd.com/EquipmentArmorDisplay.aspx?ItemName=Lamellar%20(leather))         |     1    | 1560 gold pieces |
| [formula alembic](https://www.d20pfsrd.com/magic-items/wondrous-items/e-g/formula-alembic/)                                                                                                     |     1    |  200 gold pieces |
| [hybridization funnel](https://www.d20pfsrd.com/magic-items/wondrous-items/h-l/hybridization-funnel/)                                                                                           |     1    |  300 gold pieces |
| +1 [ring of protection](https://www.d20pfsrd.com/magic-items/rings/ring-of-protection/)                                                                                                         |     1    | 2000 gold pieces |
| masterwork [longbow](https://www.d20pfsrd.com/equipment/weapons/weapon-descriptions/longbow)                                                                                                    |     1    |  375 gold pieces |
| [arrow, dye](https://www.d20pfsrd.com/equipment/weapons/weapon-descriptions/ammunition/ammunition-bow-arrows-common/ammunition-bow-arrow-dye)                                                   |   100    |  100 gold pieces |
| [arrow, smoke](https://www.d20pfsrd.com/equipment/weapons/weapon-descriptions/ammunition/ammunition-bow-arrows-common/ammunition-bow-arrows-smokescreen)                                        |    20    |  200 gold pieces |
| [arrow, tangleshot](https://www.d20pfsrd.com/equipment/weapons/weapon-descriptions/ammunition/ammunition-bow-arrows-common/ammunition-bow-arrow-tanglefoot)                                     |    25    |  500 gold pieces |
| [alkali flask](https://www.aonprd.com/EquipmentMiscDisplay.aspx?ItemName=Alkali%20flask)                                                                                                        |     3    |   60 gold pieces |
| [artokus](https://www.aonprd.com/EquipmentMiscDisplay.aspx?ItemName=Artokus%27s%20fire)                                                                                                         |     1    |  100 gold pieces |
| [ghast retch flask](https://www.aonprd.com/EquipmentMiscDisplay.aspx?ItemName=Ghast%20retch%20flask)                                                                                            |     5    |  250 gold pieces |
| [incendiary catalyst](https://aonprd.com/EquipmentMiscDisplay.aspx?ItemName=Incendiary%20catalyst)                                                                                              |    10    |  400 gold pieces |
| [mineral acid](https://www.aonprd.com/EquipmentMiscDisplay.aspx?ItemName=Mineral%20acid)                                                                                                        |     2    |  100 gold pieces |
| [thunderstone](https://www.aonprd.com/EquipmentMiscDisplay.aspx?ItemName=Thunderstone)                                                                                                          |     3    |   90 gold pieces |
| [handy haversack](https://www.aonprd.com/MagicWondrousDisplay.aspx?FinalName=Handy%20Haversack)                                                                                                 |     1    | 2000 gold pieces |



<--- [goz mask](https://aonprd.com/MagicWondrousDisplay.aspx?FinalName=Goz%20Mask) --->

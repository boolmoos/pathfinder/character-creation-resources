
## classes

[cleric](https://www.d20pfsrd.com/classes/core-classes/cleric/) ([evangelist](https://www.d20pfsrd.com/classes/core-classes/cleric/archetypes/paizo-cleric-archetypes/evangelist))

**favored class:** cleric  
**deity:** [onmyuza](https://www.aonprd.com/DeityDisplay.aspx?ItemName=Onmyuza)  
**alignment:** chaotic neutral  

## race

human
- **bonus feat**
  - select one additional feat at level 1
- **skilled**
  - gain 1 extra skill per character level 

## traits

- [sacred conduit](https://www.aonprd.com/TraitDisplay.aspx?ItemName=Sacred%20Conduit)
  - +1 trait bonus to the difficulty check of your channel energy ability
- [twitchy](https://www.aonprd.com/TraitDisplay.aspx?ItemName=Twitchy)
  - +2 trait bonus to initiative
  - +1 trait bonus to reflex saves
- [holy tattoo](https://www.aonprd.com/TraitDisplay.aspx?ItemName=Holy%20Tattoo)
  - you have a tattoo that serves as your holy symbol

## drawbacks

- [paranoid](https://www.aonprd.com/TraitDisplay.aspx?ItemName=Paranoid)
  - the difficulty check for other characters to aid you is increased from 10 to 15

## attributes

|              | **str** | **dex** | **con** | **int** | **wis** | **cha** |
|-------------:|:-------:|:-------:|:-------:|:-------:|:-------:|:-------:|
| **score**    |    8    |   14    |   14    |    8    |   15    |   18    |
| **modifier** |   -1    |   +2    |   +2    |   -1    |   +2    |   +4    |

## statistic

**hit points:** 41 = 26 (class) + 10 (constitution) + 5 (favored class)  
**initiative:** +10 = 4 (charisma) + 2 (trait) + 4 (improved initiative)  
**base attack bonus:** +3  
**combat maneuver defense:** 14 = 10 + 3 (base attack bonus) - 1 (strength) + 2 (dexterity) 

### armor class

|          type         |   total   |                         components                           |
|----------------------:|:---------:|:-------------------------------------------------------------|
|          **standard** |     23    | 10 + 5 (armor) + 5 (shield) + 2 (dexterity) + 1 (deflection) |
|             **touch** |     13    | 10 + 2 (dexterity) + 1 (deflection)                          |
|       **flat-footed** |     16    | 10 + 5 (armor) + 1 (deflection)                              |
| **flat-footed touch** |     11    | 10 + 1 (deflection)                                          |

### saving throws

|     type      |   total   |                  components                     |
|--------------:|:---------:|:------------------------------------------------|
| **fortitude** |     7     |  4 + 2 (attribute) + 1 (resistance)             |
|    **reflex** |     5     |  1 + 2 (attribute) + 1 (resistance) + 1 (trait) |
|      **will** |     7     |  4 + 2 (attribute) + 1 (resistance)             |

### skills

**total:** 10 = 10 (class) - 5 (intelligence) + 5 (skilled)

|                                                                                 type | total |                    components                                                                               | class skill | untrained |
|-------------------------------------------------------------------------------------:|:-----:|:------------------------------------------------------------------------------------------------------------|:-----------:|:---------:|
|                 [acrobatics](https://www.aonprd.com/Skills.aspx?ItemName=Acrobatics) |       | 0 (ranks)                                                                                                   |     no      |    yes    | 
|                     [appraise](https://www.aonprd.com/Skills.aspx?ItemName=Appraise) |       | 0 (ranks)                                                                                                   |    yes      |    yes    |
|                           [bluff](https://www.aonprd.com/Skills.aspx?ItemName=Bluff) |   +5  | 1 (ranks) + 4 (charisma)                                                                                    |     no      |    yes    |
|                           [climb](https://www.aonprd.com/Skills.aspx?ItemName=Climb) |       | 0 (ranks)                                                                                                   |     no      |    yes    |
|                           [craft](https://www.aonprd.com/Skills.aspx?ItemName=Craft) |       | 0 (ranks)                                                                                                   |    yes      |    yes    |
|                   [diplomacy](https://www.aonprd.com/Skills.aspx?ItemName=Diplomacy) |   +8  | 1 (ranks) + 4 (charisma) + 3 (class skill)                                                                  |    yes      |    yes    |
|       [disable device](https://www.aonprd.com/Skills.aspx?ItemName=Disable%20Device) |       | 0 (ranks)                                                                                                   |     no      |     no    |
|                     [disguise](https://www.aonprd.com/Skills.aspx?ItemName=Disguise) |       | 0 (ranks)                                                                                                   |     no      |    yes    |
|         [escape artist](https://www.aonprd.com/Skills.aspx?ItemName=Escape%20Artist) |       | 0 (ranks)                                                                                                   |     no      |    yes    |
|                               [fly](https://www.aonprd.com/Skills.aspx?ItemName=Fly) |       | 0 (ranks)                                                                                                   |     no      |    yes    |
|         [handle animal](https://www.aonprd.com/Skills.aspx?ItemName=Handle%20Animal) |       | 0 (ranks)                                                                                                   |     no      |     no    |
|                             [heal](https://www.aonprd.com/Skills.aspx?ItemName=Heal) |       | 0 (ranks)                                                                                                   |    yes      |    yes    |
|                 [intimidate](https://www.aonprd.com/Skills.aspx?ItemName=Intimidate) |       | 0 (ranks)                                                                                                   |     no      |    yes    |
|          [knowledge (arcana)](https://www.aonprd.com/Skills.aspx?ItemName=Knowledge) |       | 0 (ranks)                                                                                                   |    yes      |     no    |
|   [knowledge (dungeoneering)](https://www.aonprd.com/Skills.aspx?ItemName=Knowledge) |       | 0 (ranks)                                                                                                   |     no      |     no    |
|     [knowledge (engineering)](https://www.aonprd.com/Skills.aspx?ItemName=Knowledge) |       | 0 (ranks)                                                                                                   |     no      |     no    |
|       [knowledge (geography)](https://www.aonprd.com/Skills.aspx?ItemName=Knowledge) |       | 0 (ranks)                                                                                                   |     no      |     no    |
|         [knowledge (history)](https://www.aonprd.com/Skills.aspx?ItemName=Knowledge) |       | 0 (ranks)                                                                                                   |    yes      |     no    |
|           [knowledge (local)](https://www.aonprd.com/Skills.aspx?ItemName=Knowledge) |       | 0 (ranks)                                                                                                   |     no      |     no    |
|          [knowledge (nature)](https://www.aonprd.com/Skills.aspx?ItemName=Knowledge) |       | 0 (ranks)                                                                                                   |     no      |     no    |
|        [knowledge (nobility)](https://www.aonprd.com/Skills.aspx?ItemName=Knowledge) |       | 0 (ranks)                                                                                                   |    yes      |     no    |
|           [knowledge (plane)](https://www.aonprd.com/Skills.aspx?ItemName=Knowledge) |   +3  | 1 (ranks) - 1 (intelligence) + 3 (class skill)                                                              |    yes      |     no    |
|        [knowledge (religion)](https://www.aonprd.com/Skills.aspx?ItemName=Knowledge) |   +3  | 1 (ranks) - 1 (intelligence) + 3 (class skill)                                                              |    yes      |     no    |
|               [linguistics](https://www.aonprd.com/Skills.aspx?ItemName=Linguistics) |  n/a  | 0 (ranks)                                                                                                   |    yes      |     no    |
|                 [perception](https://www.aonprd.com/Skills.aspx?ItemName=Perception) |   +3  | 1 (ranks) + 2 (wisdom)                                                                                      |     no      |    yes    |
|                       [perform](https://www.aonprd.com/Skills.aspx?ItemName=Perform) |   +8  | 1 (ranks) + 4 (charisma) + 3 (class skill)                                                                  |    yes      |    yes    |
|                 [profession](https://www.aonprd.com/Skills.aspx?ItemName=Profession) |       | 0 (ranks)                                                                                                   |    yes      |     no    |
|                             [ride](https://www.aonprd.com/Skills.aspx?ItemName=Ride) |       | 0 (ranks)                                                                                                   |     no      |    yes    |
|           [sense motive](https://www.aonprd.com/Skills.aspx?ItemName=Sense%20Motive) |   +6  | 1 (ranks) + 2 (wisdom) + 3 (class skill)                                                                    |    yes      |    yes    |
|   [sleight of hand](https://www.aonprd.com/Skills.aspx?ItemName=Sleight%20of%20Hand) |       | 0 (ranks)                                                                                                   |     no      |     no    |
|                 [spellcraft](https://www.aonprd.com/Skills.aspx?ItemName=Spellcraft) |   +3  | 1 (ranks) - 1 (intelligence) + 3 (class skill)                                                              |    yes      |    yes    |
|                       [stealth](https://www.aonprd.com/Skills.aspx?ItemName=Stealth) |       | 0 (ranks)                                                                                                   |     no      |    yes    |
|                     [survival](https://www.aonprd.com/Skills.aspx?ItemName=Survival) |       | 0 (ranks) + 0 (widsom)                                                                                      |     no      |    yes    |
|                             [swim](https://www.aonprd.com/Skills.aspx?ItemName=Swim) |       | 0 (ranks)                                                                                                   |     no      |    yes    |
| [use magic device](https://www.aonprd.com/Skills.aspx?ItemName=Use%20Magic%20Device) |       | 0 (ranks)                                                                                                   |     no      |     no    |

## class features

- [domain](https://www.d20pfsrd.com/classes/Core-Classes/Cleric/#Domains): [revelry](https://www.aonprd.com/DomainDisplay.aspx?ItemName=Chaos)
- [variant channel negative energy (wine)](https://www.aonprd.com/ClericVariantChanneling.aspx) (1d8, DC 18) 7/day
- [single minded](https://www.d20pfsrd.com/classes/core-classes/Cleric/archetypes/paizo-cleric-archetypes/evangelist/#Single-Minded)
- [public speaker](https://www.d20pfsrd.com/classes/core-classes/Cleric/archetypes/paizo-cleric-archetypes/evangelist/#Public_Speaker)
- [sermoonic performance](https://www.d20pfsrd.com/classes/core-classes/Cleric/archetypes/paizo-cleric-archetypes/evangelist/#Sermonic_Performance) 15 rounds/day
  - [countersong](https://www.d20pfsrd.com/classes/core-classes/bard#TOC-Countersong-Su-)
  - [facinate](https://www.d20pfsrd.com/classes/core-classes/bard#TOC-Fascinate-Su-)
  - [inspire courage](https://www.d20pfsrd.com/classes/core-classes/bard#TOC-Inspire-Courage-Su-) +2
- [spontaneous casting](https://www.d20pfsrd.com/classes/core-classes/Cleric/archetypes/paizo-cleric-archetypes/evangelist/#Spontaneous_Casting)

## feats

- [improved channel](https://www.aonprd.com/FeatDisplay.aspx?ItemName=Improved%20Channel)
- [improved initiative](https://www.aonprd.com/FeatDisplay.aspx?ItemName=Improved%20Initiative)
- [noble scion](https://www.aonprd.com/FeatDisplay.aspx?ItemName=Noble%20Scion): war
- [selective channeling](https://www.aonprd.com/FeatDisplay.aspx?ItemName=Selective%20Channeling)

## spells

|            level |   1   |   2   |   3   |
|-----------------:|:-----:|:-----:|:-----:|
| extracts per day | 4 + 1 | 3 + 1 | 1 + 1 |

### 1st level
  - [protection from law](https://www.aonprd.com/SpellDisplay.aspx?ItemName=protection%20from%20law)\*
  - [bless](https://www.aonprd.com/SpellDisplay.aspx?ItemName=bless)
  - [bless](https://www.aonprd.com/SpellDisplay.aspx?ItemName=bless)
  - [moment of greatness](https://www.aonprd.com/SpellDisplay.aspx?ItemName=moment%20of%20greatness)
  - [moment of greatness](https://www.aonprd.com/SpellDisplay.aspx?ItemName=moment%20of%20greatness)
  - [obscuring mist](https://www.aonprd.com/SpellDisplay.aspx?ItemName=obscuring%20mist)
### 2nd level
  - [hideous laughter](https://www.aonprd.com/SpellDisplay.aspx?ItemName=Hideous%20Laughter)\*
  - [ashen path](https://www.aonprd.com/SpellDisplay.aspx?ItemName=ashen%20path)
  - [protection from evil, communal](https://www.aonprd.com/SpellDisplay.aspx?ItemName=Protection%20from%20Evil,%20Communal)
  - [cloud of seasickness](https://www.aonprd.com/SpellDisplay.aspx?ItemName=cloud%20of%20seasickness) 
### 3rd level
  - [good hope](https://www.aonprd.com/SpellDisplay.aspx?ItemName=Good%20Hope)\*
  - [channel the gift](https://www.aonprd.com/SpellDisplay.aspx?ItemName=Channel%20the%20Gift) 

:::{note}

- spells denoted with \* are granted by the characters domain
:::

## equipment

|                                                                                           equipment                                                                                             | quantity |      value       |
|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------:|:--------:|-----------------:|
| +1 [cloak of resistance](https://www.d20pfsrd.com/magic-items/wondrous-items/c-d/cloak-of-resistance/)                                                                                          |     1    | 1000 gold pieces |
| +1 [darkleaf](https://www.d20pfsrd.com/equipmenT/special-materials/#Darkleaf_Cloth) [lamellar leather](https://www.aonprd.com/EquipmentArmorDisplay.aspx?ItemName=Lamellar%20(leather))         |     1    | 1560 gold pieces |
| +1 [darkwood](https://www.d20pfsrd.com/equipmenT/special-materials/#Darkwood) [tower shield](https://www.aonprd.com/EquipmentArmorDisplay.aspx?ItemName=Tower)                                  |     1    | 1930 gold pieces |
| +1 lesser metamagic rod of [encouraging spell](https://www.aonprd.com/FeatDisplay.aspx?ItemName=Encouraging%20Spell)                                                                            |     1    | 3000 gold pieces |
| +1 [ring of protection](https://www.d20pfsrd.com/magic-items/rings/ring-of-protection/)                                                                                                         |     1    | 2000 gold pieces |
| wand of [infernal healing](https://www.aonprd.com/SpellDisplay.aspx?ItemName=infernal%20healing) (25 charges)                                                                                   |     1    |  225 gold pieces |
| wand of [lesser restoration](https://www.aonprd.com/SpellDisplay.aspx?ItemName=restoration) (25 charges)                                                                                        |     1    |  225 gold pieces |
|                                                                                                                                                                                                 |          |  110 gold pieces |

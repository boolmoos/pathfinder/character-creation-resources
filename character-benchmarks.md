# Character benchmarks

:::{contents}
 :depth: 1
 :local:
:::

## Introduction

Opposing creature statistics are the context by which character statistic are
evaluated.

- Is your attack bonus sufficient to deal damage consistently during combat?  
  That depends on the distribution of creature armor class values that you might
  reasonably expect to engage in combat.
- Is your armor class sufficient to avoid depleting party healing resources?
  That depends on the distribution of creature attack bonuses that you might
  reasonably expect to engage in combat.

While players may not explicitly consider these standards at character creation,
they're certainly applied during the course of the game and especially during
combat. Ordinarily, some level of investment is required to meet these
thresholds. In addition, for most character statistics, there comes a point when
improved proficiency yields little additional utility or investment in the
character statistic exhibits diminishing returns.

A character is assembled from finite resources and those resources are
constrained such that a single character cannot excel at everything. This
encourages collaboration among players to accomplish goals and allows characters
to develop distinct (and hopefully complementary) mechanical identities.

Unfortunately, it's not uncommon for players to allocate resources such that

- their character can do many things, but not at a level of proficiency to be
  effective or reliable
- their character is overly centralized on a single mechanic

In either case, the impact is often that

- the player is frequently unable to contribute or participate meaningfully
  during large spans of a game session.
- the party is left without reliable access to one or more critical functions
  and is either unable to accomplish mission objectives or is forced to expend
  excessive resources to do so.

The purpose of this document is to provides players with the means to understand
the potential return on investment when designing characters and avoid those
undesirable outcomes.

For each character level **N**, for a series of character statistics, we
consider the median value of a creature's opposing statistic among creatures of
challenge rating **N+2** (considering bestiaries I-IV). To this value, the
corresponding standard deviation value is then added, thereby providing a rough
approximation of the 70th percentile creature statistic. This value is taken as
a basis for comparison for the corresponding character statistic. This is
intended to serve as a moderately conservative estimate of the figure for 
enemies posing a significant threat to a party of adventurers of level **N**.

Data and summary statistics has been drawn from 
[this](https://docs.google.com/spreadsheets/d/1E2-s8weiulPoBQjdI05LBzOUToyoZIdSsLKxHAvf8F8/edit#gid=3)
aggregation of creature statistics.

## Armor class

:::{contents}
 :depth: 1
 :local:
:::

A character's armor class competes with a creature's attack bonus(es). Many
creatures' attacks are categorized as either primary or secondary attacks, where
the former uses a creature's full base attack bonus and the latter uses the
creature's base attack bonus - 5.

### Methodology

We consider the character armor class necessary to avoid a creature's attack 80%
of the time, 50% of the time, and 20% of the time, for both primary and secondary
attacks.

### Results

<table align="center">
 <tr style="font-weight: bold">
  <td rowspan="2"> level </td>
  <td rowspan="2"> median </td>
  <td rowspan="2"> standard<br>deviation</td>
  <td rowspan="2"> benchmark </td>
  <td colspan="2"> 80% </td>
  <td colspan="2"> 50% </td>
  <td colspan="2"> 20% </td>
 </tr>
 <tr style="font-weight: bold">
  <td> primary </td>
  <td> secondary </td>
  <td> primary </td>
  <td> secondary </td>
  <td> primary </td>
  <td> secondary </td>
 </tr>
 <tr>
  <td> 5 </td>
  <td> 13 </td>
  <td> 4.29 </td>
  <td> 18 </td>
  <td> 35 </td>
  <td> 30 </td>
  <td> 29 </td>
  <td> 24 </td>
  <td> 24 </td>
  <td> 19 </td>
 </tr>
 <tr>
  <td> 6 </td>
  <td> 14 </td>
  <td> 4.63 </td>
  <td> 19 </td>
  <td> 36 </td>
  <td> 31 </td>
  <td> 30 </td>
  <td> 25 </td>
  <td> 25 </td>
  <td> 20 </td>
 </tr>
 <tr>
  <td> 7 </td>
  <td> 16 </td>
  <td> 4.28 </td>
  <td> 21 </td>
  <td> 38 </td>
  <td> 33 </td>
  <td> 32 </td>
  <td> 27 </td>
  <td> 27 </td>
  <td> 22 </td>
 </tr>
 <tr>
  <td> 8 </td>
  <td> 17 </td>
  <td> 6.51 </td>
  <td> 24 </td>
  <td> 41 </td>
  <td> 36 </td>
  <td> 35 </td>
  <td> 30 </td>
  <td> 30 </td>
  <td> 25 </td>
 </tr>
 <tr>
  <td> 9 </td>
  <td> 19 </td>
  <td> 6.36 </td>
  <td> 26 </td>
  <td> 43 </td>
  <td> 38 </td>
  <td> 37 </td>
  <td> 32 </td>
  <td> 32 </td>
  <td> 27 </td>
 </tr>
 <tr>
  <td> 10 </td>
  <td> 20 </td>
  <td> 7.78 </td>
  <td> 28 </td>
  <td> 45 </td>
  <td> 40 </td>
  <td> 39 </td>
  <td> 34 </td>
  <td> 33 </td>
  <td> 28 </td>
 </tr>
 <tr>
  <td> 11 </td>
  <td> 22 </td>
  <td> 8.39 </td>
  <td> 31 </td>
  <td> 48 </td>
  <td> 43 </td>
  <td> 42 </td>
  <td> 37 </td>
  <td> 37 </td>
  <td> 32 </td>
 </tr>
 <tr>
  <td> 12 </td>
  <td> 23 </td>
  <td> 8.90 </td>
  <td> 32 </td>
  <td> 49 </td>
  <td> 44 </td>
  <td> 43 </td>
  <td> 38 </td>
  <td> 38 </td>
  <td> 33 </td>
 </tr>
</table>

### Conclusions

The 80% and 50% goals against a threatening creature's primary attack are 
daunting challenges, and lie outside of what can be reasonably expected to be
achievable with level-appropriate equipment alone; some combination of class
abilities, feats, and spell effects are likely achieve those values.

We encourage characters generally (but especially those that are intended to
enter melee combat) to dedicate sufficient resources to achieve the 20% primary
attack benchmark. While this level of investment (alone) is unlikely to keep a
character standing for long during a challenging fight, it will often be
sufficient to mitigate much of the potential damage in the (less threatening)
encounters leading up to it.

As an alternative to investing heavily on improving armor class beyond this
benchmark, parties can incorporate tactics that allow them to apply multiple
(stacking) debilitating conditions to enemies, e.g. conditions  which

- render the enemy unable to attack (cowering, dazed, frightened, fascinated,
  nauseated, panicked, paralyzed, stunned)
- potentially nullify enemy attacks (blind, confused)
- reduce the number of attacks (staggered)
- apply a penalty to the enemy attack rolls (dazzled, entagled, grappled,
  prone, shaken, sickened)
- apply a penalty to the enemy attributes used to determine atack bonuses
  (entagled, exhausted, fatigue)

Each such effect applied to the enemy effectively increases each party member's
survivability.

Failing to meet the 20% secondary attack benchmark is cause for some concern and
warrants consideration when planning a characters tactics in combat. Alternative
damage mitigation strategies (e.g. defensive spells such as mirror image) are
strongly recommended for such characters.

Naturally, minimizing the number of rounds of combat a creature has to attack
party members by killing it as fast as reasonable achievable is also an
excellent strategy.

## Attack bonus

:::{contents}
 :depth: 1
 :local:
:::

A character's attack bonus is opposed by a creature's armor class. Depending on
the properties and circumstances of the attack, this could resolve considering
a creatures standard armor class, flat-footed armor class, touch armor class, or
flat-footed touch armor class.

### Methodology

Distributions of standard, flat-footed, and touch armor classes are readily
available and are considered as benchmarks for comparison.

Given these benchmark values, opposing character statistic thresholds are
calculated such that an associated action has an 80% and 50% chance of success,
both under normal single attacks and dual attacks, e.g., two-weapon fighting or
rapid shot.

### Results

#### Standard

<table align="center">
 <tr style="font-weight: bold">
  <td rowspan="2"> level </td>
  <td rowspan="2"> median </td>
  <td rowspan="2"> standard<br>deviation</td>
  <td rowspan="2"> benchmark </td>
  <td colspan="2"> 80% </td>
  <td colspan="2"> 50% </td>
 </tr>
 <tr style="font-weight: bold">
  <td> single </td>
  <td> dual </td>
  <td> single </td>
  <td> dual </td>
 </tr>
 <tr>
  <td> 5 </td>
  <td> 20 </td>
  <td> 3.33 </td>
  <td> 24 </td>
  <td> 19 </td>
  <td> 14 </td>
  <td> 13 </td>
  <td> 8   </td>
 </tr>
 <tr>
  <td> 6 </td>
  <td> 21 </td>
  <td> 2.58 </td>
  <td> 24 </td>
  <td> 19 </td>
  <td> 14 </td>
  <td> 13 </td>
  <td> 8   </td>
 </tr>
 <tr>
  <td> 7 </td>
  <td> 23 </td>
  <td> 3.27 </td>
  <td> 27 </td>
  <td> 22 </td>
  <td> 17 </td>
  <td> 16 </td>
  <td> 11   </td>
 </tr>
 <tr>
  <td> 8 </td>
  <td> 24 </td>
  <td> 2.85 </td>
  <td> 27 </td>
  <td> 22 </td>
  <td> 17 </td>
  <td> 16 </td>
  <td> 11   </td>
 </tr>
 <tr>
  <td> 9 </td>
  <td> 25 </td>
  <td> 3.96 </td>
  <td> 29 </td>
  <td> 24 </td>
  <td> 19 </td>
  <td> 18 </td>
  <td> 13   </td>
 </tr>
 <tr>
  <td> 10 </td>
  <td> 27 </td>
  <td> 2.90 </td>
  <td> 30 </td>
  <td> 25 </td>
  <td> 20 </td>
  <td> 19 </td>
  <td> 14   </td>
 </tr>
 <tr>
  <td> 11 </td>
  <td> 28 </td>
  <td> 4.04 </td>
  <td> 33 </td>
  <td> 28 </td>
  <td> 23 </td>
  <td> 22 </td>
  <td> 17   </td>
 </tr>
 <tr>
  <td> 12 </td>
  <td> 29 </td>
  <td> 4.70 </td>
  <td> 34 </td>
  <td> 29 </td>
  <td> 24 </td>
  <td> 23 </td>
  <td> 18   </td>
 </tr>
</table>
 

#### Flat-footed

<table align="center">
 <tr style="font-weight: bold">
  <td rowspan="2"> level </td>
  <td rowspan="2"> median </td>
  <td rowspan="2"> standard<br>deviation</td>
  <td rowspan="2"> benchmark </td>
  <td colspan="2"> 80% </td>
  <td colspan="2"> 50% </td>
 </tr>
 <tr style="font-weight: bold">
  <td> single </td>
  <td> dual </td>
  <td> single </td>
  <td> dual </td>
 </tr>
 <tr>
  <td> 5 </td>
  <td> 17 </td>
  <td> 3.36 </td>
  <td> 21 </td>
  <td> 16 </td>
  <td> 11 </td>
  <td> 11 </td>
  <td> 6 </td>
 </tr>
 <tr>
  <td> 6 </td>
  <td> 18 </td>
  <td> 2.98 </td>
  <td> 21 </td>
  <td> 16 </td>
  <td> 11 </td>
  <td> 11 </td>
  <td> 6 </td>
 </tr>
 <tr>
  <td> 7 </td>
  <td> 20 </td>
  <td> 3.58 </td>
  <td> 24 </td>
  <td> 19 </td>
  <td> 14 </td>
  <td> 14 </td>
  <td> 9 </td>
 </tr>
 <tr>
  <td> 8 </td>
  <td> 21 </td>
  <td> 3.76 </td>
  <td> 25 </td>
  <td> 20 </td>
  <td> 15 </td>
  <td> 15 </td>
  <td> 10 </td>
 </tr>
 <tr>
  <td> 9 </td>
  <td> 22 </td>
  <td> 4.51 </td>
  <td> 27 </td>
  <td> 22 </td>
  <td> 17 </td>
  <td> 17 </td>
  <td> 12 </td>
 </tr>
 <tr>
  <td> 10 </td>
  <td> 24 </td>
  <td> 3.94 </td>
  <td> 28 </td>
  <td> 23 </td>
  <td> 18 </td>
  <td> 18 </td>
  <td> 13 </td>
 </tr>
 <tr>
  <td> 11 </td>
  <td> 24 </td>
  <td> 4.75 </td>
  <td> 29 </td>
  <td> 24 </td>
  <td> 19 </td>
  <td> 19 </td>
  <td> 14 </td>
 </tr>
 <tr>
  <td> 12 </td>
  <td> 26 </td>
  <td> 5.51 </td>
  <td> 32 </td>
  <td> 27 </td>
  <td> 22 </td>
  <td> 22 </td>
  <td> 17 </td>
 </td>
</table>

#### Touch

<table align="center">
 <tr style="font-weight: bold">
  <td rowspan="2"> level </td>
  <td rowspan="2"> median </td>
  <td rowspan="2"> standard<br>deviation</td>
  <td rowspan="2"> benchmark </td>
  <td colspan="2"> 80% </td>
  <td colspan="2"> 50% </td>
 </tr>
 <tr style="font-weight: bold">
  <td> single </td>
  <td> dual </td>
  <td> single </td>
  <td> dual </td>
 </tr>
 <tr>
  <td> 5 </td>
  <td> 12 </td>
  <td> 3.68 </td>
  <td> 16 </td>
  <td> 11 </td>
  <td> 6 </td>
  <td> 6 </td>
  <td> 1 </td>
 </tr>
 <tr>
  <td> 6 </td>
  <td> 11 </td>
  <td> 3.70 </td>
  <td> 15 </td>
  <td> 10 </td>
  <td> 5 </td>
  <td> 5 </td>
  <td> 0 </td>
 </tr>
 <tr>
  <td> 7 </td>
  <td> 12 </td>
  <td> 3.82 </td>
  <td> 16 </td>
  <td> 11 </td>
  <td> 6 </td>
  <td> 6 </td>
  <td> 1 </td>
 </tr>
 <tr>
  <td> 8 </td>
  <td> 11 </td>
  <td> 4.21 </td>
  <td> 16 </td>
  <td> 11 </td>
  <td> 6 </td>
  <td> 6 </td>
  <td> 1 </td>
 </tr>
 <tr>
  <td> 9 </td>
  <td> 11 </td>
  <td> 4.15 </td>
  <td> 16 </td>
  <td> 11 </td>
  <td> 6 </td>
  <td> 6 </td>
  <td> 1 </td>
 </tr>
 <tr>
  <td> 10 </td>
  <td> 11.5 </td>
  <td> 4.39 </td>
  <td> 16 </td>
  <td> 11 </td>
  <td> 6 </td>
  <td> 6 </td>
  <td> 1 </td>
 </tr>
 <tr>
  <td> 11 </td>
  <td> 10 </td>
  <td> 5.24 </td>
  <td> 16 </td>
  <td> 11 </td>
  <td> 6 </td>
  <td> 6 </td>
  <td> 1 </td>
 </tr>
 <tr>
  <td> 12 </td>
  <td> 11 </td>
  <td> 5.47 </td>
  <td> 17 </td>
  <td> 12 </td>
  <td> 7 </td>
  <td> 7 </td>
  <td> 2 </td>
 </tr>
</table> 

### Conclusions

Rendering an enemy flat-footed improves a character's odds of delivering damage
by ~15% across the span of character levels analyzed, on average. There is
more variance in flat-footed armor class than standard armor class and enemies
who's armor class is unaffected by being flat-footed are not teribly uncommon.

The benchmarks for touch attacks are essentially static over the range of
character levels analyzed. The negative trend in median values is offset by
increasing standard deviation. This increase in variance means that as
character level increases, it becomes more and more important to meet or exceed
the 80% benchmark. This is effectively a given for 3/4 base attack bonus
classes, but may require minor investment for 1/2 base attack bonus classes.

The penalty associated with dual attack abilities is generally -2, whereas the
the corresponding attack bonus target is 5 points lower for the 80% and
50% categories, respectively, which more than offsets the penalty. Characters
leveraging these abilities are 15% more likely to successfully land an
attack on a given turn. That said, it doesn't necessarily translate to higher
expected damage per turn. The extra damage associated with wielding weapons with
two hands and with two-handed weapon dice typically offsets the additional
damage potential associated with multiple successful attacks.

Confounding factors hindering a comparative analysis between the stratgies
include

- effects that improve or reduce incident damage per attack (e.g., sneak attack
  or damage reduction, respectively)
- effeccts that add a constant number of additional attacks per turn (e.g., the
  haste spell)
- the dependence of dual attack abilities on the full attack action.

The latter is to the particular detriment of characters designed for melee
combat, who more frequently need to expend a move action to reposition,
limiting them to standard action attacks or charge actions.

For attack-oriented characters, I encourage characters to strive for the 80%
threshold, but that goal is difficult to achieve, especially at low levels. The
standard deviation in standard armor class values is relatively consistent over
the character levels analyzed, so maintaining a consistent benchmark defect
over a character's career isn't the end of the world (although that will still
likely require significant consistent investement). Failing to meet the 50%
threshold should be taken as cause for concern.

For melee characters, I encourage players to include potential flanking bonuses
into consideration for this purpose. For ranged characters, I encourage players
to include potential point-blank shot bonuses into consideration for this
purpose. Beyond that, when considering party composition, support in the form of
enemy debuff effects, such as trip, entangle, or blind (rendering the enemy
flat-footed), or modest buffs can significantly improve the reliability of
damage-oriented characters, especially when taken in combination.

## Combat maneuver bonus

:::{contents}
 :depth: 1
 :local:
:::

A character's combat maneuver bonus competes with a creature's combat maneuver
defense value.

### Methodology

Opposing character statistic thresholds are calculated such that a combat
maneuver has an 80% and 50% chance of success, both under normal conditions and
when at "advantage", i.e., when two dice are rolled and the better of the two
values is taken.

### Results

<table align="center">
 <tr style="font-weight: bold">
  <td rowspan="2"> level </td>
  <td rowspan="2"> median </td>
  <td rowspan="2"> standard<br>deviation</td>
  <td rowspan="2"> benchmark </td>
  <td colspan="2"> 80% </td>
  <td colspan="2"> 50% </td>
 </tr>
 <tr style="font-weight: bold">
  <td> standard </td>
  <td> advantage </td>
  <td> standard </td>
  <td> advantage </td>
 </tr>
 <tr>
  <td> 5 </td>
  <td> 26 </td>
  <td> 4.01 </td>
  <td> 30 </td>
  <td> 25 </td>
  <td> 21 </td>
  <td> 19 </td>
  <td> 14   </td>
 </tr>
 <tr>
  <td> 6 </td>
  <td> 28 </td>
  <td> 4.42 </td>
  <td> 33 </td>
  <td> 28 </td>
  <td> 24 </td>
  <td> 22 </td>
  <td> 17   </td>
 </tr>
 <tr>
  <td> 7 </td>
  <td> 30 </td>
  <td> 5.31 </td>
  <td> 36 </td>
  <td> 31 </td>
  <td> 27 </td>
  <td> 25 </td>
  <td> 20   </td>
 </tr>
 <tr>
  <td> 8 </td>
  <td> 31.5 </td>
  <td> 4.38 </td>
  <td> 36 </td>
  <td> 31 </td>
  <td> 27 </td>
  <td> 25 </td>
  <td> 20   </td>
 </tr>
 <tr>
  <td> 9 </td>
  <td> 34 </td>
  <td> 5.73 </td>
  <td> 40 </td>
  <td> 35 </td>
  <td> 31 </td>
  <td> 29 </td>
  <td> 24   </td>
 </tr>
 <tr>
  <td> 10 </td>
  <td> 35 </td>
  <td> 5.52 </td>
  <td> 41 </td>
  <td> 36 </td>
  <td> 32 </td>
  <td> 30 </td>
  <td> 25   </td>
 </tr>
 <tr>
  <td> 11 </td>
  <td> 38 </td>
  <td> 5.91 </td>
  <td> 44 </td>
  <td> 39 </td>
  <td> 35 </td>
  <td> 33 </td>
  <td> 28   </td>
 </tr>
 <tr>
  <td> 12 </td>
  <td> 39 </td>
  <td> 7.78 </td>
  <td> 47 </td>
  <td> 42 </td>
  <td> 37 </td>
  <td> 36 </td>
  <td> 31   </td>
 </tr>
</table>

### Conclusions

Combat maneuvers rarely end combat directly and compete in the action economy
with attack actions. As such, reliability is paramount. As such, for characters
employing a combat maneuver as a primary offensive tactic, I encourage players
to aim to meet the 80% combat maneuver bonus threshold (at least) in order to
have favorable odds of contributing consistently in combats that pose a 
significant threat to the party.

For characters employing a combat maneuver as a secondary offensive tactic,
e.g., as a rider on an attack mechanism that already meaningfully impacts the
combat or as a fall back should a more favorable tactic prove ineffective, I
encourage players to aim to meet the 50% combat maneuver bonus threshold.

The target combat maneuver bonus values are beyond what characters will achieve
incidentally or even with modest investment. Failing those levels of proficiency,
the tactic is insufficiently reliable and should be relegated to incidentally
and/or opportunistic usage.

## Damage

:::{contents}
 :depth: 1
 :local:
:::

A character's damage competes with a creature's hit points.

### Methodology

In most (reasonably constructed) parties, there are multiple characters designed
to inflict hitpoint damage. Two to three characters is typical. Given that many
abilities have usage budgets expressed in rounds per day, protracted combat is 
generally considered to be to the detriment of the player characters.

Individual character damage benchmark values were calculated assuming two and
half (2.5) party members contributing damage, depleting the reference creature
hit points in three rounds of combat.

Dealing hit point damage is (almost always) dependent on a successful attack
roll (on the character's behalf) or failed saving throw (on the creature's
behalf). The probability of inflicting damage effectively scales the the
benchmark value. Character target thresholds for 80%, 50%, and 30% scaling have
been tabulated.

### Results

| level |   median  |  standard<br>deviation   | benchmark |    80%    |    50%    |    30%    |
|:-----:|:---------:|:------------------------:|:---------:|:---------:|:---------:|:---------:|
|   5   |     85    |    12.94                 |     14    |     17    |    27     |    47     |
|   6   |     95    |    14.95                 |     15    |     19    |    30     |    50     |
|   7   |    114    |    14.03                 |     18    |     22    |    35     |    60     |
|   8   |    126    |    20.87                 |     20    |     25    |    40     |    67     |
|   9   |    147    |    18.03                 |     23    |     28    |    45     |    77     |
|  10   |    161    |    23.00                 |     25    |     31    |    50     |    84     |
|  11   |    184    |    26.18                 |     29    |     36    |    57     |    97     |
|  12   |    195    |    28.10                 |     30    |     38    |    60     |   100     |

### Conclusion

Incorporating the probability of success has a dramatic effect on the target
value and that effect scales non-linearly with decreasing probability. At the
lowest levels considered, the target thresholds are achievable with highly
specialized characters, but this becomes more and more untenable as level
increases.

Player's stand to benefit from being mindful of this when

1. comparing the relative merits of effects providing damage bonuses and effects
   providing bonuses to attack rolls
2. considering the application of effects that penalize attack bonus in exchange
   for damage bonuses (e.g. power attack, deadly aim)

## Effective caster level

:::{contents}
 :depth: 1
 :local:
:::

A character's effective caster level competes with a creature's spell
resistance, for the subset of spells subject to spell resistance, to determine
whether the spell effect is applied.

### Methodology

Caster level targets have been tabulated for the 80% and 50% success thresholds
considering creatures of the reference challenge rating with spell resistance.
The fraction of creatures of the reference challenge rating with spell
resistance has also been tabulated.

### Results

| level | distribution  |   median  |  standard<br>deviation  | benchmark |    80%    |    50%    |
|:-----:|:-------------:|:---------:|:-----------------------:|:---------:|:---------:|:---------:|
|   5   |      20%      |     18    |    1.50                 |    20     |    15     |     9     |
|   6   |      22%      |     19    |    0.92                 |    20     |    15     |     9     |
|   7   |      22%      |     20    |    1.37                 |    22     |    17     |    11     |
|   8   |      36%      |     21    |    0.94                 |    22     |    17     |    11     |
|   9   |      37%      |     22    |    1.01                 |    23     |    18     |    12     |
|  10   |      44%      |     23    |    0.89                 |    24     |    19     |    13     |
|  11   |      47%      |     24    |    0.86                 |    25     |    20     |    14     |
|  12   |      66%      |     25    |     0.0                 |    25     |    20     |    14     |

### Conclusion

The fraction of creatures with spell resistance is significant over the entire
range of character levels analyzed but becomes more common with increating
level, accounting for a more than a third of monsters at level 8 and
approximately half of monsters at level 11.

The investment to exceeding the target thresholds becomes less taxing with
increasing level as

1. the difference between a character's base caster level and the benchmark
   spell resistance vaue become smaller AND 
2. resources to increase caster level (or effective caster level) become more
   reasily available

However, at no level is this level of investment modest. Reaching the 80%
threshold will typically include contributions from racial abilities (e.g. the
elves '**Elven magic**, the aasimars' **Crusading magic**), feats (e.g. **spell
penetration**, **knowledgable spellcaster**), and effects that consume
additional daily resources (e.g. **piercing spell** metamagic, the
**sure casting** spell).

An alternative strategy is to avoid this investment and instead to prepare for
creatures with high spell resistance by incorporating spells that

1. don't allow for spell resistance (e.g. much of the conjuration school)
2. provide beneficial effects for allies rather than apply detrimental effects
   to enemies (i.e. buffs)

into the character's spells known or spells prepared.

## Stealth

:::{contents}
 :depth: 1
 :local:
:::

A character's stealth bonus competes with a creature's perception bonus. The
resolution of a stealth check is somewhat unusual in that it's one the few
mechanics in pathfinder in which uses opposed rolls.

### Methodology

Calculating the probability of success for opposed roles is a bit more involved
than against static offsets. The probability of success for a character with
stealth bonus **S** oppposed by a creature with a perception bonus **P** may be
expressed as:

:::{math}
P(S, P) = \frac{1}{400} \sum_{i=1}^{20} \sum_{j=1}^{20}
          \begin{array}{c}
          1 , & \text{if } (S + j) > (P + i) \\
          0 , & \text{if } (S + j) \leq (P + i)
          \end{array}
:::

This expression can be rearranged as:

:::{math}
P(S, P) = \frac{1}{400} \sum_{i=1}^{20} \sum_{j=1}^{20}
          \begin{array}{c}
          1 , & \text{if } (S - P) > (i - j) \\
          0 , & \text{if } (S - P) \leq (i - j)
          \end{array}
:::

Recognizing that

- {math}`S - P` is constant throughout the summations
- {math}`i - j` includes repeated values

we can collect terms and tabulate the cumulative probability of each value of {math}`S - P`.

|**S - P** |   0   |   1   |   2   |   3   |   4   |   5   |   6   |   7   |   8   |   9   | 
|:--------:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
| **Prob** | 47.50 | 52.50 | 57.25 | 61.75 | 66.00 | 70.00 | 73.75 | 77.25 | 80.50 | 83.50 | 

|**S - P** |   10  |   11  |   12  |   13  |   14  |   15  |   16  |   17  |   18  |   19  |
|:--------:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|
| **Prob** | 86.25 | 88.75 | 91.00 | 93.00 | 94.75 | 96.25 | 97.50 | 98.50 | 99.25 | 99.75 |

Stealth bonus targets have been tabulated for the 90%, 80%, and 70% success
thresholds considering creatures of the reference challenge rating. 

### Results

| level |  median  |  standard<br>deviation  | benchmark |    90%    |    80%    |    70%    |
|:-----:|:--------:|:-----------------------:|:---------:|:---------:|:---------:|:---------:|
|   5   |    13    |    5.92                 |    19     |    31     |    27     |    24     |
|   6   |    14    |    6.57                 |    21     |    33     |    29     |    26     |
|   7   |    16    |    6.47                 |    23     |    35     |    31     |    28     |
|   8   |    18    |    7.60                 |    26     |    38     |    34     |    31     |
|   9   |    19    |    8.07                 |    28     |    40     |    36     |    33     |
|  10   |    21    |    6.85                 |    28     |    40     |    36     |    33     |
|  11   |   21.5   |    9.51                 |    31     |    43     |    39     |    36     |
|  12   |    23    |    7.66                 |    31     |    43     |    39     |    36     |

### Conclusion

## Intimidate

:::{contents}
 :depth: 1
 :local:
:::

Intimidation allows a character to demoralize an opposing creature in combat,
imparting the shaken status condition. A character's intimidate skill opposed to
a difficulty check considering the creature's hit dice and wisdom modifier.

The duration of the shaken status condition is dictated by the difference
between the initiator's skill check and the difficulty check, starting at one
round and increaseing by one round for every 5 points of difference. Attempting
to intimidate a creature of a larger size category imposes a -4 penalty to the
character's roll. Subsequent attempt to demoralize a creature suffer a -5
penalty for each previous attempt, regardless of whether those attempts were
successful.

### Methodology

Summary statistics for this value are not readily available. In leui of this
information, we've approximated benchmark values by considering the
distributions of the individual components of the difficulty check (which were
readily available) and assuming the respective distributions of these values are
uncorrelated. The accuracy of these benchmarks could be improved by taking the
time to collect those summary statistics, but that work have been left to future
versions of the document (or industrious players). 

Given this benchmark value, opposing character statistic thresholds are
calculated such that a combat maneuver has an 80% and 50% chance of success,
both under normal conditions and when at "advantage", i.e., when two dice are
rolled and the better of the two values is taken, conservatively assuming the 
creature targetted is larger than character attempting to intimidate.

### Results

<table align="center">
 <tr style="font-weight: bold">
  <td rowspan="2"> level </td>
  <td colspan="2"> hit dice </td>
  <td colspan="2"> wisdom </td>
  <td rowspan="2"> benchmark </td>
  <td colspan="2"> 80% </td>
  <td colspan="2"> 50% </td>
 </tr>
 <tr style="font-weight: bold">
  <td> median </td>
  <td> standard<br>deviation</td>
  <td> median </td>
  <td> standard<br>deviation</td>
  <td> standard </td>
  <td> advantage </td>
  <td> standard </td>
  <td> advantage </td>
 </tr>
 <tr>
  <td> 5 </td>
  <td> 9 </td>
  <td> 1.25 </td>
  <td> 13 </td>
  <td> 3.49 </td>
  <td> 28 </td>
  <td> 23 </td>
  <td> 18 </td>
  <td> 17 </td>
  <td> 12   </td>
 </tr>
 <tr>
  <td> 6 </td>
  <td> 10 </td>
  <td> 1.49 </td>
  <td> 14 </td>
  <td> 3.12 </td>
  <td> 30 </td>
  <td> 25 </td>
  <td> 20 </td>
  <td> 19 </td>
  <td> 14   </td>
 </tr>
 <tr>
  <td> 7 </td>
  <td> 12 </td>
  <td> 1.83 </td>
  <td> 14 </td>
  <td> 3.37 </td>
  <td> 32 </td>
  <td> 27 </td>
  <td> 22 </td>
  <td> 21 </td>
  <td> 16   </td>
 </tr>
 <tr>
  <td> 8 </td>
  <td> 13 </td>
  <td> 2.16 </td>
  <td> 14 </td>
  <td> 3.13 </td>
  <td> 34 </td>
  <td> 29 </td>
  <td> 24 </td>
  <td> 23 </td>
  <td> 18   </td>
 </tr>
 <tr>
  <td> 9 </td>
  <td> 14 </td>
  <td> 1.84 </td>
  <td> 14 </td>
  <td> 3.87 </td>
  <td> 34 </td>
  <td> 29 </td>
  <td> 24 </td>
  <td> 23 </td>
  <td> 18   </td>
 </tr>
 <tr>
  <td> 10 </td>
  <td> 15 </td>
  <td> 2.07 </td>
  <td> 15 </td>
  <td> 3.34 </td>
  <td> 36 </td>
  <td> 31 </td>
  <td> 26 </td>
  <td> 25 </td>
  <td> 20   </td>
 </tr>
 <tr>
  <td> 11 </td>
  <td> 16 </td>
  <td> 3.86 </td>
  <td> 16 </td>
  <td> 4.35 </td>
  <td> 39 </td>
  <td> 34 </td>
  <td> 29 </td>
  <td> 28 </td>
  <td> 23   </td>
 </tr>
 <tr>
  <td> 12 </td>
  <td> 17 </td>
  <td> 2.31 </td>
  <td> 18 </td>
  <td> 4.40 </td>
  <td> 40 </td>
  <td> 35 </td>
  <td> 30 </td>
  <td> 29 </td>
  <td> 24   </td>
 </tr>
</table>

### Conclusion

Although max skill ranks in intimidate are not sufficient on thier own, the 50%
benchmark is certainly achievable given the wide availability of bonuses to the
skill, making demoralization an attractive secondary combat tactic.

The benchmark value outstrips the bonus from maximum skill ranks by roughly a
factor of two to one. As such, this tactic requires modest investment to remain
effective into higher levels.


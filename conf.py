# Configuration file for the Sphinx documentation builder.
#
# For the full list of built-in configuration values, see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Project information -----------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#project-information

project = 'Character creation resources'
copyright = 'Copyright 2022, 2023 Austin McCartney'
author = 'Austin McCartney'
release = '0.0.0'

# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration

extensions = ["myst_parser"]

templates_path = ['_templates']
exclude_patterns = ['_build', 'Thumbs.db', '.DS_Store']

# -- Options for HTML output -------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#options-for-html-output

import sphinx_theme
html_theme = 'sphinx_alice_theme'
html_theme_path = ["themes", ]

master_doc = 'index'
myst_enable_extensions = [
  "amsmath",
  "colon_fence",
  'deflist',
  "dollarmath",
  "replacements",
  "smartquotes"]

from sphinx.writers.html import HTMLTranslator

class PatchedHTMLTranslator(HTMLTranslator):
   def visit_reference(self, node):
      if node.get('newtab') or not (node.get('target') or node.get('internal')
         or 'refuri' not in node):
            node['target'] = '_blank'
      super().visit_reference(node)

def setup(app):
    app.set_translator('html', PatchedHTMLTranslator)
